package cisbenchmark

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

type Check struct {
	ID          string
	Description string
	State       string
}

type Result struct {
	ID          string
	Description string
	Checks      []Check
}

type CISReport struct {
	Version       string
	Total         int
	Pass          int
	Fail          int
	Skip          int
	Warn          int
	NotApplicable int
	Results       []Result
}

func generateCISReportSummary(cmd *cobra.Command, flags *flags) error {
	reports, err := collectReports()
	if err != nil {
		return err
	}

	if err := writeSummary(reports); err != nil {
		return err
	}

	if err := writeFailures(reports); err != nil {
		return err
	}

	return nil
}

func writeSummary(reports map[string]*CISReport) error {

	var b strings.Builder
	for clustername, report := range reports {
		fmt.Fprintf(&b, "%s\n\tTotal: %d\n\tPass: %d\n\tFail: %d\n\tSkip: %d\n\tWarn: %d\n\tNotApplicable: %d\n\tprofile: %v\n",
			clustername, report.Total, report.Pass, report.Fail, report.Skip, report.Warn, report.NotApplicable, report.Version)

		checkMap := splitChecks(report)
		for state, checks := range checkMap {
			if state != "pass" {
				printChecks(&b, checks, state)
			}
		}
	}

	fileName := "reports/summary.txt"
	fmt.Print(b.String())
	return os.WriteFile(fileName, []byte(b.String()), 0644)
}

func writeFailures(reports map[string]*CISReport) error {
	var b strings.Builder
	for clustername, report := range reports {
		fmt.Fprintf(&b, "%s\nFail: %d\n", clustername, report.Fail)

		checkMap := splitChecks(report)
		for state, checks := range checkMap {
			if strings.HasPrefix(state, "fa") { // fail , failure
				printChecks(&b, checks, state)
			}
		}
	}

	fileName := "reports/fail.txt"
	fmt.Printf("\n** Failures **\n%s", b.String())
	return os.WriteFile(fileName, []byte(b.String()), 0644)
}

func splitChecks(report *CISReport) map[string][]Check {
	checkMap := make(map[string][]Check)
	for _, result := range report.Results {
		for _, check := range result.Checks {
			if checkMap[check.State] == nil {
				checkMap[check.State] = make([]Check, 0)
			}
			checkMap[check.State] = append(checkMap[check.State], check)
		}
	}
	return checkMap
}

func printChecks(b *strings.Builder, checks []Check, state string) {
	fmt.Fprintf(b, "\tChecks in state %s:\n", state)

	for _, check := range checks {
		if check.State == state {
			fmt.Fprintf(b, "\t\t%-10s %s\n", check.ID, check.Description)
		}
	}
}

func collectReports() (map[string]*CISReport, error) {
	dirName := "./reports/"
	files, err := ioutil.ReadDir(dirName)
	if err != nil {
		return nil, err
	}

	cisReportList := make(map[string]*CISReport)

	for _, file := range files {
		if !strings.HasSuffix(file.Name(), ".json") {
			continue
		}
		data, _ := ioutil.ReadFile(dirName + file.Name())

		var report CISReport
		err = json.Unmarshal(data, &report)
		if err != nil {
			return nil, err
		}

		cisReportList[fileNameWithoutExtension(file.Name())] = &report
	}
	return cisReportList, nil
}

func fileNameWithoutExtension(fileName string) string {
	if pos := strings.IndexByte(fileName, '.'); pos != -1 {
		return fileName[:pos]
	}
	return fileName
}
