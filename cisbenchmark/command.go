package cisbenchmark

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

type flags struct {
	context *string
	summary *bool
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			if *flags.summary {
				return generateCISReportSummary(cmd, &flags)
			} else {
				return generateCISReport(cmd, &flags)
			}
		},
		Use:   "get-cis-benchmark-report",
		Short: "Retrieve CIS benchmark report",
		Long:  "This command retrieves the latest report of the CIS benchmark.",
	}

	flags.context = cmd.Flags().String("context", "", "K8s context")
	flags.summary = cmd.Flags().Bool("summary", false, "Summary")

	return cmd
}

type Spec struct {
	ReportJSON       string
	LastRunTimestamp string
}

type ClusterScanReport struct {
	Spec Spec

	LastRunTimestampAsTime time.Time
}

func generateCISReport(cmd *cobra.Command, flags *flags) error {

	k8sAPI := k8s.CreateAPI(*flags.context)

	gvr := schema.GroupVersionResource{
		Group:    "cis.cattle.io",
		Version:  "v1",
		Resource: "clusterscanreports",
	}

	scanreports, err := k8sAPI.Dynamic.Resource(gvr).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return err
	}
	scanReport, err := getLatestReport(scanreports.Items)
	if err != nil {
		return err
	} else if scanReport == nil {
		log.Printf("No scanreports found on cluster")
		return nil
	}

	if _, err := os.Stat("reports"); os.IsNotExist(err) {
		err := os.Mkdir("reports", 0777)
		if err != nil {
			return err
		}
	}

	fileName := fmt.Sprintf("reports/%s-%s.json", k8sAPI.Cluster, scanReport.LastRunTimestampAsTime.Format("2006-01-02"))
	log.Printf("Create report %q", fileName)
	err = os.WriteFile(fileName, jsonPrettyPrint(scanReport.Spec.ReportJSON), 0644)

	return err
}

func getLatestReport(scanReports []unstructured.Unstructured) (latestReport *ClusterScanReport, err error) {
	for _, scanReport := range scanReports {
		var nextReport ClusterScanReport
		runtime.DefaultUnstructuredConverter.FromUnstructured(scanReport.UnstructuredContent(), &nextReport)
		dateFormat := "2006-01-02 15:04:05.999999999 +0000 UTC"
		nextReportTime, err := time.Parse(dateFormat, strings.Split(nextReport.Spec.LastRunTimestamp, " m=")[0])
		if err != nil {
			return nil, err
		}
		nextReport.LastRunTimestampAsTime = nextReportTime

		if latestReport == nil {
			latestReport = &nextReport
		} else {
			if nextReport.LastRunTimestampAsTime.After(latestReport.LastRunTimestampAsTime) {
				latestReport = &nextReport
			}
		}
	}
	return
}

func jsonPrettyPrint(in string) []byte {
	var out bytes.Buffer
	err := json.Indent(&out, []byte(in), "", "\t")
	if err != nil {
		return out.Bytes()
	}
	return out.Bytes()
}
