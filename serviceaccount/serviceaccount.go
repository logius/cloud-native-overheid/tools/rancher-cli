package serviceaccount

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"

	clusterClient "github.com/rancher/rancher/pkg/client/generated/cluster/v3"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/xanzy/go-gitlab"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// Creates RBACDefinition
// The purpose is to create a definition that dynamically adds rolebindings in a namespace.
// If a namespace is created that conforms to the namespace-selector, the rbac-manager operator adds the configured rolebindings to the namespace
// The namespace-selector selects namespaces that belongs to a ranger project
//
// The RBACDefinition  gives the ServiceAccount of a customer:
// 1) A new clusterrole is created that allows read/update/delete in namespaces
// 2) A RBACDefinition is created, the configured serviceaccount is given
//   - a clusterroleBinding to create namespaces clusterwide
//   - a rolebinding to read/update/delete the current namespace
//   - an admin roleBinding for the customer namespaces
//
// # If a namespaces is created that contains the ranger project label (belonging to the project), a role is bound to the serviceaccount that also allows the serviceaccount to manage that namespaces
//
// NB: It is possible to create a namespaces with a wrong ranger project label (or no label at all). The namespace will be created but the service account can't do anything with it
func createRbacDefinitionforProject(k8sAPI *k8s.API, customerConfig *config.Customer, project *managementClient.Project) error {
	serviceAccountName := makeDeployerName(customerConfig.Name, false)
	serviceAccountNamespace := strings.ToLower(project.Name) + "-services"

	rbacdefinitionName := strings.ToLower(project.Name) + "-rbac"
	roleBindingClusterroleName := "rbacmanager-namespace-rud"

	projectIdSuffix, err := extractRancherProjectIdSuffix(project)
	if err != nil {
		return err
	}
	projectNamespaceClusterroleName := fmt.Sprintf("%s-namespaces-edit", projectIdSuffix)
	err = clusterroleExists(k8sAPI, projectNamespaceClusterroleName)
	if err != nil {
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "rbacmanager.reactiveops.io",
		Version:  "v1beta1",
		Resource: "rbacdefinitions",
	}

	projectNamespaceSelector := map[string]interface{}{
		"matchExpressions": []map[string]interface{}{
			{
				"key":      "rbacmanager",
				"operator": "NotIn",
				"values": []string{
					//the customer serviceacount doesn't get rolebindings in the -service namespace
					//this label is set during the project creation where the <customer>-service ns is also created
					"services",
				},
			},
		},
		"matchLabels": map[string]interface{}{
			"field.cattle.io/projectId": projectIdSuffix,
		},
	}

	rbacdefinition := unstructured.Unstructured{
		Object: map[string]interface{}{
			"kind":       "RBACDefinition",
			"apiVersion": "rbacmanager.reactiveops.io/v1beta1",
			"metadata": map[string]interface{}{
				"name": rbacdefinitionName,
			},
			"rbacBindings": []map[string]interface{}{
				{
					"name": "mananage-ns",
					"subjects": []map[string]interface{}{
						{
							"kind":      "ServiceAccount",
							"name":      serviceAccountName,
							"namespace": serviceAccountNamespace,
						},
					},
					"clusterRoleBindings": []map[string]interface{}{
						{
							"clusterRole": "create-ns",
						},
						{
							"clusterRole": "k8s-crds-view",
						},
						{
							"clusterRole": projectNamespaceClusterroleName,
						},
					},
					"roleBindings": []map[string]interface{}{
						{
							"clusterRole":       "admin",
							"namespaceSelector": projectNamespaceSelector,
						},
						{
							"clusterRole":       roleBindingClusterroleName,
							"namespaceSelector": projectNamespaceSelector,
						},
						{
							"clusterRole": "view",
							"namespace":   serviceAccountNamespace,
						},
					},
				},
			},
		},
	}

	// used in rolebindings of project namespaces
	err = createClusterrole(k8sAPI, roleBindingClusterroleName)
	if err != nil {
		return err
	}

	currentObject, err := k8sAPI.Dynamic.Resource(gvr).Get(context.TODO(), rbacdefinitionName, metav1.GetOptions{})
	if err != nil {
		log.Printf("Create %q", rbacdefinitionName)
		_, err = k8sAPI.Dynamic.Resource(gvr).Create(context.TODO(), &rbacdefinition, metav1.CreateOptions{})
	} else {
		rbacdefinition.SetResourceVersion(currentObject.GetResourceVersion())
		updateObject, err := k8sAPI.Dynamic.Resource(gvr).Update(context.TODO(), &rbacdefinition, metav1.UpdateOptions{})
		if err != nil {
			return err
		}

		if currentObject.GetResourceVersion() != updateObject.GetResourceVersion() {
			log.Printf("Updated %q", rbacdefinitionName)
		}
	}

	return nil
}

// manageServiceAccount creates or updates service account for the deployer role
func manageServiceAccount(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabClientPeer *gitlab.Client, customerConfig *config.Customer,
	tokenLifetime int, projectName string, clusterAdmin bool, clusterAdminNamespace string) error {

	serviceAccountName := makeDeployerName(customerConfig.Name, clusterAdmin)

	var namespace string
	if clusterAdmin == true {
		namespace = clusterAdminNamespace
		_, err := k8sAPI.Clientset.CoreV1().Namespaces().Get(context.TODO(), clusterAdminNamespace, metav1.GetOptions{})
		if err != nil {
			log.Printf("Creating namespace %v ", clusterAdminNamespace)
			ns := &corev1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name: clusterAdminNamespace,
				},
			}
			_, err = k8sAPI.Clientset.CoreV1().Namespaces().Create(context.TODO(), ns, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		}
	} else {
		namespace = strings.ToLower(projectName) + "-services"
	}

	serviceAccount, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace).Get(context.TODO(), serviceAccountName, metav1.GetOptions{})
	if err != nil {
		log.Printf("Create serviceaccount %v in namespace %s", serviceAccountName, namespace)
		serviceAccount := &corev1.ServiceAccount{
			ObjectMeta: metav1.ObjectMeta{
				Name: serviceAccountName,
			},
		}
		serviceAccount, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace).Create(context.TODO(), serviceAccount, metav1.CreateOptions{})
		if err != nil {
			return err
		}
		return saveGitLabServiceAccounts(k8sAPI, gitlabClient, gitlabClientPeer, customerConfig.GitLab, serviceAccount, projectName, clusterAdmin)
	}

	_, err = deleteExpiredServiceAccountSecrets(k8sAPI, serviceAccount, tokenLifetime)
	if err != nil {
		return err
	}

	return saveGitLabServiceAccounts(k8sAPI, gitlabClient, gitlabClientPeer, customerConfig.GitLab, serviceAccount, projectName, clusterAdmin)
}

// removes all rolebindings where the subject is the customer sa in customer namespaces that are not managed (e.g. k8s system or the rbac-manager)
func removeNonRbacOperatorRolebindings(k8sAPI *k8s.API, customerConfig *config.Customer, projectConfig config.Project, namespaceCollection *clusterClient.NamespaceCollection) error {
	serviceAccountName := makeDeployerName(customerConfig.Name, false)

	serviceAccount, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(projectConfig.Name+"-services").Get(context.TODO(), serviceAccountName, metav1.GetOptions{})
	if err != nil {
		return err
	}

	for _, namespace := range namespaceCollection.Data {
		roleBindings, err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			return err
		}

		for _, roleBinding := range roleBindings.Items {
			if len(roleBinding.OwnerReferences) > 0 {
				continue
			}
			for _, subject := range roleBinding.Subjects {
				if subject.Kind == rbacv1.ServiceAccountKind && subject.Name == serviceAccount.Name && subject.Namespace == serviceAccount.Namespace {
					log.Printf("Delete rolebinding %q from namespace %q", roleBinding.Name, namespace.Name)
					err = k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Delete(context.TODO(), roleBinding.Name, metav1.DeleteOptions{})
					if err != nil {
						return err
					}
					break
				}
			}
		}
	}
	return nil
}

func getCurrentServiceAccountSecrets(k8sAPI *k8s.API, serviceAccount *corev1.ServiceAccount) ([]corev1.Secret, error) {

	secrets := make([]corev1.Secret, 0)
	listOpt := metav1.ListOptions{
		FieldSelector: fmt.Sprintf("type=%s", corev1.SecretTypeServiceAccountToken),
	}
	secretList, err := k8sAPI.Clientset.CoreV1().Secrets(serviceAccount.Namespace).List(context.TODO(), listOpt)
	if err != nil {
		return nil, err
	}
	for _, secret := range secretList.Items {
		if isTokenForServiceAccount(&secret, serviceAccount) {
			secrets = append(secrets, secret)
		}
	}
	return secrets, nil
}

// The bool return value indicates if we have found a secret that was not expired.
func deleteExpiredServiceAccountSecrets(k8sAPI *k8s.API, serviceAccount *corev1.ServiceAccount, tokenLifetime int) (bool, error) {
	k8sVersionMinor, err := strconv.Atoi(k8sAPI.Version.Minor)
	if k8sAPI.Version.Major == "1" && k8sVersionMinor >= 24 {
		log.Printf("K8S version is 1.24 or higher. Ignoring auto-generated ServiceAccount secrets", k8sAPI.Cluster)
		return true, nil
	}
	if err != nil {
		return true, err
	}
	secretList, err := getCurrentServiceAccountSecrets(k8sAPI, serviceAccount)
	if err != nil {
		return true, err
	}
	expired := true

	serviceAccountTokenLifetime := time.Duration(tokenLifetime) * time.Minute

	for _, secret := range secretList {
		someTimeAgo := time.Now().Add(0 - serviceAccountTokenLifetime)
		if secret.CreationTimestamp.Time.Before(someTimeAgo) {
			// K8s will update the list of secrets in the serviceaccount.
			log.Printf("Delete deployer token %q with timestamp %v from namespace %q", secret.Name, secret.CreationTimestamp, secret.Namespace)
			err := k8sAPI.Clientset.CoreV1().Secrets(secret.Namespace).Delete(context.TODO(), secret.Name, metav1.DeleteOptions{})
			if err != nil {
				return expired, err
			}
		} else {
			expired = false
		}
	}
	return expired, nil
}

// creates the clusterrole used in the rolebindings in customer project namespaces
func createClusterrole(k8sAPI *k8s.API, clusterRoleName string) error {

	clusterRole := &rbacv1.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{
			Name: clusterRoleName,
		},
		TypeMeta: metav1.TypeMeta{
			APIVersion: rbacv1.SchemeGroupVersion.String(),
			Kind:       "ClusterRole",
		},
		Rules: []rbacv1.PolicyRule{
			{
				APIGroups: []string{""},
				Resources: []string{"namespaces"},
				Verbs:     []string{"get", "update", "patch", "delete", "list", "watch"},
			},
		},
	}

	currentObject, err := k8sAPI.Clientset.RbacV1().ClusterRoles().Get(context.TODO(), clusterRoleName, metav1.GetOptions{})
	if err != nil {
		log.Printf("Create clusterrole %q", clusterRoleName)
		_, err = k8sAPI.Clientset.RbacV1().ClusterRoles().Create(context.TODO(), clusterRole, metav1.CreateOptions{})
	} else {
		clusterRole.SetResourceVersion(currentObject.GetResourceVersion())
		updateObject, err := k8sAPI.Clientset.RbacV1().ClusterRoles().Update(context.TODO(), clusterRole, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
		if currentObject.GetResourceVersion() != updateObject.GetResourceVersion() {
			log.Printf("Updated clusterrole %q", clusterRoleName)
		}
	}
	return nil
}

func makeDeployerName(baseName string, clusterAdmin bool) string {

	var serviceAccountName string
	if clusterAdmin == true {
		serviceAccountName = "provisioner"
	} else {
		serviceAccountName = baseName + "-deployer"
	}
	return serviceAccountName
}

func extractRancherProjectIdSuffix(project *managementClient.Project) (string, error) {
	parts := strings.Split(project.ID, ":")

	if len(parts) == 2 {
		return parts[1], nil
	}
	return "", fmt.Errorf("unable to get project ID suffix for project %q", project.Name)
}

// creates the clusteradmin rolebinding
func createClusterAdminCrb(k8sAPI *k8s.API, clusterAdminNamespace string) error {

	clusterroleBinding := &rbacv1.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name: "provisioner-clusteradmin",
		},
		TypeMeta: metav1.TypeMeta{
			APIVersion: rbacv1.SchemeGroupVersion.String(),
			Kind:       "ClusterRoleBinding",
		},
		Subjects: []rbacv1.Subject{
			{
				Kind:      "ServiceAccount",
				Name:      "provisioner",
				Namespace: clusterAdminNamespace,
			},
		},
		RoleRef: rbacv1.RoleRef{

			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "ClusterRole",
			Name:     "cluster-admin",
		},
	}

	currentObject, err := k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Get(context.TODO(), "provisioner-clusteradmin", metav1.GetOptions{})
	if err != nil {
		log.Printf("Create clusterrolebinding provisioner-clusteradmin")
		_, err = k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Create(context.TODO(), clusterroleBinding, metav1.CreateOptions{})
	} else {
		clusterroleBinding.SetResourceVersion(currentObject.GetResourceVersion())
		updateObject, err := k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Update(context.TODO(), clusterroleBinding, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
		if currentObject.GetResourceVersion() != updateObject.GetResourceVersion() {
			log.Printf("Updated clusterrolebinding provisioner-clusteradmin")
		}
	}
	return nil
}

func clusterroleExists(k8sAPI *k8s.API, clusterrolename string) error {

	_, err := k8sAPI.Clientset.RbacV1().ClusterRoles().Get(context.TODO(), clusterrolename, metav1.GetOptions{})
	if err != nil {
		log.Printf("Coulnd't retrieve ClusterRole %s", clusterrolename)
		return err
	}
	return nil
}
