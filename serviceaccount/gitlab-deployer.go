package serviceaccount

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	authv1 "k8s.io/api/authentication/v1"
	"k8s.io/utils/pointer"
	"log"
	"sigs.k8s.io/yaml"

	"github.com/xanzy/go-gitlab"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8stypes "k8s.io/client-go/tools/clientcmd/api/v1"
)

// saveGitLabServiceAccounts saves the token for the deployer service account in GitLab
func saveGitLabServiceAccounts(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabClientPeer *gitlab.Client, gitlabConfig config.GitLab, serviceAccount *v1.ServiceAccount, projectName string, clusterAdmin bool) error {
	token, err := generateTokenByServiceAccount(k8sAPI, serviceAccount)
	if err != nil {
		return err
	}
	for _, gitlabGroupConfig := range gitlabConfig.Groups {

		gitlabGroup, err := gitlabclient.GetGitLabGroup(gitlabClient, gitlabGroupConfig.Name)
		if err != nil {
			return err
		}
		err = saveGitLabServiceAccountInGroup(k8sAPI, gitlabClient, gitlabGroup, token.Status.Token, projectName)
		if err != nil {
			return err
		}
		if gitlabClientPeer != nil {
			log.Printf("Trying to update serviceaccount in secondary Gitlab.")
			err = saveGitLabServiceAccountInGroup(k8sAPI, gitlabClientPeer, gitlabGroup, token.Status.Token, projectName)
			if err != nil {
				log.Printf("Skipping: Unable to update serviceaccount. err=%v", err)
			}
		}

	}
	return nil
}

func saveGitLabServiceAccountInGroup(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group,
	deployerToken string, projectName string) error {

	var clusterName string
	if projectName == "" {
		clusterName = k8sAPI.Cluster
	} else {
		clusterName = k8sAPI.Cluster + "." + projectName
	}

	if err := saveGitLabServiceAccountInGroupVariables(k8sAPI, gitlabClient, gitlabGroup, deployerToken, clusterName); err != nil {
		return err
	}
	return removeGroupCluster(k8sAPI, gitlabClient, gitlabGroup, clusterName)
}

func removeGroupCluster(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, clusterName string) error {

	groupCluster := getGroupCluster(gitlabClient, gitlabGroup, clusterName)

	if groupCluster != nil {
		log.Printf("Delete cluster %q from group %q", k8sAPI.Cluster, gitlabGroup.FullPath)
		_, err := gitlabClient.GroupCluster.DeleteCluster(gitlabGroup.ID, groupCluster.ID)
		return err
	}
	return nil
}

/*
Create multiple KUBECONFIG variables for each unique environment.
The logical key for the GitLab group variable is key + environment_scope.
*/
func saveGitLabServiceAccountInGroupVariables(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group,
	deployerToken string, clusterName string) error {

	apiServerURL := k8sAPI.Config.Host + k8sAPI.Config.APIPath
	scope := clusterName + "*"
	k8sConfigYaml := createK8sConfig(k8sAPI.Cluster, apiServerURL, deployerToken)
	varName := "KUBECONFIG"
	log.Printf("Update %s variable in group %q", varName, gitlabGroup.FullPath)
	return gitlabclient.UpdateGroupVariable(gitlabClient, gitlabGroup, varName, string(k8sConfigYaml), scope, gitlab.FileVariableType)
}

func saveGitLabAggregatedKubeconfig(gitlabClient *gitlab.Client, gitlabGroupName string,
	k8sConfigYaml []byte, scope string) error {

	gitlabGroup, err := gitlabclient.GetGitLabGroup(gitlabClient, gitlabGroupName)
	if err != nil {
		return err
	}
	varName := "KUBECONFIG"
	log.Printf("Update %s variable with scope=%s in group %q", varName, scope, gitlabGroup.FullPath)
	return gitlabclient.UpdateGroupVariable(gitlabClient, gitlabGroup, varName, string(k8sConfigYaml), scope, gitlab.FileVariableType)
}

func createK8sConfig(clusterName string, apiServerURL string, deployerToken string) []byte {
	k8sConfig := k8stypes.Config{
		APIVersion: "v1",
		Kind:       "Config",
		Clusters: []k8stypes.NamedCluster{
			{
				Name: clusterName,
				Cluster: k8stypes.Cluster{
					Server: apiServerURL,
				},
			},
		},
		Contexts: []k8stypes.NamedContext{
			{
				Name: clusterName,
				Context: k8stypes.Context{
					Cluster:  clusterName,
					AuthInfo: clusterName,
				},
			},
		},
		AuthInfos: []k8stypes.NamedAuthInfo{
			{
				Name: clusterName,
				AuthInfo: k8stypes.AuthInfo{
					Token: deployerToken,
				},
			},
		},
		CurrentContext: clusterName,
	}
	jsonData, _ := json.Marshal(k8sConfig)
	yamlData, _ := yaml.JSONToYAML(jsonData)
	return yamlData
}

func CreateAggregatedK8sConfig(namedClusters []k8stypes.NamedCluster, namedContexts []k8stypes.NamedContext, namedAuthInfos []k8stypes.NamedAuthInfo) []byte {
	k8sConfig := k8stypes.Config{
		APIVersion: "v1",
		Kind:       "Config",
		Clusters:   namedClusters,
		Contexts:   namedContexts,
		AuthInfos:  namedAuthInfos,
	}
	jsonData, _ := json.Marshal(k8sConfig)
	yamlData, _ := yaml.JSONToYAML(jsonData)
	return yamlData
}

func getGroupCluster(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, clusterName string) *gitlab.GroupCluster {

	groupClusters, _, _ := gitlabClient.GroupCluster.ListClusters(gitlabGroup.ID)
	for _, groupCluster := range groupClusters {
		if groupCluster.Name == clusterName {
			return groupCluster
		}
	}

	return nil
}

func generateTokenByServiceAccount(k8sAPI *k8s.API, serviceAccount *v1.ServiceAccount) (*authv1.TokenRequest, error) {
	request := &authv1.TokenRequest{
		Spec: authv1.TokenRequestSpec{
			ExpirationSeconds: pointer.Int64(60 * 60 * 96), // 72 hours
		},
	}

	token, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(serviceAccount.Namespace).CreateToken(context.TODO(), serviceAccount.Name, request, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	return token, nil
}

func getDeployerToken(k8sAPI *k8s.API, serviceAccount *v1.ServiceAccount) (string, error) {

	listOpt := metav1.ListOptions{
		FieldSelector: fmt.Sprintf("type=%s", v1.SecretTypeServiceAccountToken),
	}
	secretList, err := k8sAPI.Clientset.CoreV1().Secrets(serviceAccount.Namespace).List(context.TODO(), listOpt)
	if err != nil {
		return "", err
	}

	for _, secret := range secretList.Items {
		if isTokenForServiceAccount(&secret, serviceAccount) {
			return string(secret.Data["token"]), nil
		}
	}

	return "", nil
}

func isTokenForServiceAccount(secret *v1.Secret, serviceAccount *v1.ServiceAccount) bool {
	for key, value := range secret.Annotations {
		if key == v1.ServiceAccountNameKey && value == serviceAccount.Name {
			return true
		}
	}
	return false
}
