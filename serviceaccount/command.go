package serviceaccount

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/rancher/norman/types"
	clusterClient "github.com/rancher/rancher/pkg/client/generated/cluster/v3"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8stypes "k8s.io/client-go/tools/clientcmd/api/v1"
)

type flags struct {
	config                  *string
	context                 *string
	gitlabURL               *string
	gitlabURLPeer           *string
	rancherURL              *string
	rancherTokenUrlPair     map[string]string
	token                   *string
	tokenLifetime           *int
	clusterAdminNamespace   *string
	clusterAdminGitlabGroup *string
}

// NewCommand creates a new command
func NewCommandAggregatedK8sConfig() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return generateAggregatedKubeconfig(cmd, &flags)
		},
		Use:   "create-aggregated-k8sconfig",
		Short: "Manage K8s Service Account for GitLab deployer",
		Long:  "This command creates a service account in K8s and saves the token in a GitLab group.",
	}

	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")
	flags.gitlabURLPeer = cmd.Flags().String("gitlab-url-peer", "", "URL of secondary GitLab")
	flags.clusterAdminNamespace = cmd.Flags().String("cluster-admin-namespace", "", "namespace to deploy serviceaccount to")
	flags.clusterAdminGitlabGroup = cmd.Flags().String("gitlab-group", "", "gitlab group to save configuration to")
	cmd.Flags().StringToStringVar(&flags.rancherTokenUrlPair, "rancher-token-url", nil, "Array of nv pairs with token/url")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("cluster-admin-namespace")
	cmd.MarkFlagRequired("gitlab-group")
	cmd.MarkFlagRequired("rancher-token-url")

	return cmd
}

// NewCommand creates a new command
func NewCommandServiceAccount() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureServiceAccount(cmd, &flags)
		},
		Use:   "manage-service-account",
		Short: "Manage K8s Service Account for GitLab deployer",
		Long:  "This command creates a service account in K8s and saves the token in a GitLab group.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.context = cmd.Flags().String("context", "", "K8s context")
	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")
	flags.gitlabURLPeer = cmd.Flags().String("gitlab-url-peer", "", "URL of secondary GitLab")
	flags.tokenLifetime = cmd.Flags().Int("token-lifetime", 60, "Lifetime of token in minutes")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("rancher-url")
	cmd.MarkFlagRequired("token-lifetime")

	return cmd
}

func NewCommandAdminServiceAccount() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureAdminServiceAccount(cmd, &flags)
		},
		Use:   "manage-admin-service-account",
		Short: "Manage K8s Admin Service Account for GitLab deployer",
		Long:  "This command creates a service account in all K8s clusters and saves the tokens in a GitLab group.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")
	flags.gitlabURLPeer = cmd.Flags().String("gitlab-url-peer", "", "URL of secondary GitLab")
	flags.tokenLifetime = cmd.Flags().Int("token-lifetime", 60, "Lifetime of token in minutes")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")
	flags.clusterAdminNamespace = cmd.Flags().String("cluster-admin-namespace", "", "namespace to deploy serviceaccount to")
	flags.clusterAdminGitlabGroup = cmd.Flags().String("gitlab-group", "", "gitlab group to save configuration to")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("rancher-url")
	cmd.MarkFlagRequired("token-lifetime")
	cmd.MarkFlagRequired("cluster-admin-namespace")
	cmd.MarkFlagRequired("gitlab-group")

	return cmd
}

func generateAggregatedKubeconfig(cmd *cobra.Command, flags *flags) error {

	var cmdError error
	// GitLab API client
	gitlabClient, err := gitlabclient.NewGitLabClient(*flags.gitlabURL)
	if err != nil {
		return err
	}
	var gitlabClientPeer *gitlab.Client
	if *flags.gitlabURLPeer != "" {
		gitlabClientPeer, err = gitlabclient.NewGitLabClient(*flags.gitlabURLPeer)
		if err != nil {
			log.Printf("Ignoring: %v", err)
		}
	}

	namedClusters := make([]k8stypes.NamedCluster, 0)
	authInfos := make([]k8stypes.NamedAuthInfo, 0)
	contexts := make([]k8stypes.NamedContext, 0)

	for tokenEnv, url := range flags.rancherTokenUrlPair {
		rancherToken, res := os.LookupEnv(tokenEnv)
		if !res {
			return fmt.Errorf("Supplied token empty for %s", url)
		}
		mc, err := rancher.NewMasterClient(url, rancherToken)
		if err != nil {
			return err
		}

		clusterList, err := mc.ManagementClient.Cluster.List(&types.ListOpts{})
		if err != nil {
			return err
		}
		for _, rancherCluster := range clusterList.Data {

			k8sAPI, err := k8s.CreateClusterAdminK8sApi(mc, rancherCluster.Name, url)
			if err != nil {
				log.Printf("Failed to create cluster client for %q, err = %v", rancherCluster.Name, err)
				cmdError = fmt.Errorf("Failed to create cluster client, please check logs")
				continue
			}

			if k8sAPI != nil {
				serviceAccountName := makeDeployerName("", true)
				serviceAccount, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(*flags.clusterAdminNamespace).Get(context.TODO(), serviceAccountName, metav1.GetOptions{})
				if err != nil {
					log.Printf("Could not get service account %q in cluster %q %v", serviceAccountName, rancherCluster.Name, err)
					continue
				}

				token, err := generateTokenByServiceAccount(k8sAPI, serviceAccount)
				if err != nil {
					return err
				}

				namedClusters = append(namedClusters, k8stypes.NamedCluster{
					Name: k8sAPI.Cluster,
					Cluster: k8stypes.Cluster{
						Server: k8sAPI.Config.Host,
					}})
				authInfos = append(authInfos, k8stypes.NamedAuthInfo{
					Name: k8sAPI.Cluster,
					AuthInfo: k8stypes.AuthInfo{
						Token: token.Status.Token,
					}})
				contexts = append(contexts, k8stypes.NamedContext{
					Name: k8sAPI.Cluster,
					Context: k8stypes.Context{
						Cluster:  k8sAPI.Cluster,
						AuthInfo: k8sAPI.Cluster,
					}})

			}
		}
	}

	if len(namedClusters) > 0 {
		k8sConfigYaml := CreateAggregatedK8sConfig(namedClusters, contexts, authInfos)
		if err := saveGitLabAggregatedKubeconfig(gitlabClient, *flags.clusterAdminGitlabGroup, k8sConfigYaml, "lpc-*"); err != nil {
			return err
		}
		if gitlabClientPeer != nil {
			saveGitLabAggregatedKubeconfig(gitlabClientPeer, *flags.clusterAdminGitlabGroup, k8sConfigYaml, "lpc-*")
		}
	}

	return cmdError
}

func configureAdminServiceAccount(cmd *cobra.Command, flags *flags) error {

	var cmdError error

	// Rancher API client
	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	// GitLab API client
	gitlabClient, err := gitlabclient.NewGitLabClient(*flags.gitlabURL)
	if err != nil {
		return err
	}
	var gitlabClientPeer *gitlab.Client
	if *flags.gitlabURLPeer != "" {
		gitlabClientPeer, err = gitlabclient.NewGitLabClient(*flags.gitlabURLPeer)
		if err != nil {
			log.Printf("Ignoring: %v", err)
		}
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}
	gitLabGroupConfig := config.GitLabGroupConfig{
		Name: *flags.clusterAdminGitlabGroup,
	}
	gitlabGroups := []config.GitLabGroupConfig{gitLabGroupConfig}
	customerConfig.GitLab.Groups = gitlabGroups

	clusterList, err := mc.ManagementClient.Cluster.List(&types.ListOpts{})
	if err != nil {
		return err
	}

	for _, rancherCluster := range clusterList.Data {
		k8sAPI, err := k8s.CreateClusterAdminK8sApi(mc, rancherCluster.Name, *flags.rancherURL)
		if err != nil {
			log.Printf("Failed to create cluster client for %q, err = %v", rancherCluster.Name, err)
			cmdError = fmt.Errorf("Failed to create cluster client, please check logs")
			continue
		}

		if k8sAPI != nil {

			err = manageServiceAccount(k8sAPI, gitlabClient, gitlabClientPeer, customerConfig, *flags.tokenLifetime, "", true, *flags.clusterAdminNamespace)
			if err != nil {
				log.Printf("(Skipping) Failed to manage service account in cluster %q, err = %v", k8sAPI.Cluster, err)

				cmdError = fmt.Errorf("Failed to manage service account, please check logs")
				continue
			}
			err = createClusterAdminCrb(k8sAPI, *flags.clusterAdminNamespace)
			if err != nil {
				return err
			}
		}
	}
	return cmdError
}

func configureServiceAccount(cmd *cobra.Command, flags *flags) error {

	// Rancher API client
	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	// K8s API clients
	k8sAPI := k8s.CreateAPI(*flags.context)

	// GitLab API client
	gitlabClient, err := gitlabclient.NewGitLabClient(*flags.gitlabURL)
	if err != nil {
		return err
	}
	var gitlabClientPeer *gitlab.Client
	if *flags.gitlabURLPeer != "" {
		gitlabClientPeer, err = gitlabclient.NewGitLabClient(*flags.gitlabURLPeer)
		if err != nil {
			log.Printf("Ignoring: %v", err)
		}
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	for _, projectConfig := range customerConfig.Rancher.Projects {

		for _, clusterName := range projectConfig.Clusters {
			// only make a service account if there is a match between current k8s context and the project cluster
			if !strings.HasSuffix(clusterName, k8sAPI.Cluster) {
				log.Printf("Skipping: %v", clusterName)

				continue
			}
			log.Printf("Running: %v", clusterName)

			project, err := getRancherProject(mc, &projectConfig, clusterName)
			if err != nil {
				return err
			}

			err = manageServiceAccount(k8sAPI, gitlabClient, gitlabClientPeer, customerConfig, *flags.tokenLifetime, projectConfig.Name, false, "")
			if err != nil {
				return err
			}

			err = createRbacDefinitionforProject(k8sAPI, customerConfig, project)
			if err != nil {
				return err
			}

			namespaceCollection, err := getExistingNamespaces(mc, &projectConfig, project, clusterName)
			if err != nil {
				return err
			}

			err = removeNonRbacOperatorRolebindings(k8sAPI, customerConfig, projectConfig, namespaceCollection)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func getExistingNamespaces(mc *rancher.MasterClient, projectConfig *config.Project, project *managementClient.Project, clusterName string) (*clusterClient.NamespaceCollection, error) {
	cc, err := mc.NewClusterClient(clusterName, project.ClusterID)
	if err != nil {
		return nil, err
	}

	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"projectId": project.ID,
		},
	}
	namespaceCollection, err := cc.Namespace.List(&opts)
	if err != nil {
		return nil, err
	}
	return namespaceCollection, nil
}

func getRancherProject(mc *rancher.MasterClient, projectConfig *config.Project, clusterName string) (*managementClient.Project, error) {

	clusterID, err := mc.GetClusterID(clusterName)
	if err != nil {
		return nil, err
	}
	filters := map[string]interface{}{
		"name":      projectConfig.Name,
		"clusterId": clusterID,
	}
	projectCollection, err := mc.ManagementClient.Project.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return nil, err
	}

	if len(projectCollection.Data) > 0 {
		return &projectCollection.Data[0], nil
	}

	return nil, fmt.Errorf("could not find a Rancher project for %q in cluster %q", projectConfig.Name, clusterName)
}
