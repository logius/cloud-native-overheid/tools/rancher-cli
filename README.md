# Rancher CLI

Rancher CLI provides tools for configuring Rancher projects and RBAC. 

## configure-project

The subcommand `configure-project` creates a Rancher project using a specification in a YAML file. If the YAML file specifies namespaces, the subcommand will create these namespaces with resource quota as well. 

## configure-global-roles

The subcommand `configure-global-roles` creates global roles in Rancher. This command should only to be used while bootstrapping the Rancher environment. 

## manage-service-account

The subcommand `manage-service-account` creates a K8s service account in the services namespace and saves this in the GitLab group. 
- Each customer has a namespace with the name `<customer>-services`.
- In this namespace a service account is created with the name `<customer>-deployer`.
- The service account gets rolebindings in each namespaces of the customer, to the clusterrole as specified in param `cluster-role`. With these rolebindings the serviceaccount has sufficient permissions to perform rollout of applications. The cluster role is managed by Rancher as a global project role. 
- The token for the service account is saved in a Kubernetes environment in the GitLab group(s) of the customer. This facilitates the pipelines for authorization to the clusters. 
- The token for the service account is rotated. The lifetime is specified in the param `token-lifetime` with a default of 60 minutes. After expiry of the lifetime a new token is created. Older tokens are removed, but only if a new token was issued at least for the duration of lifetime. This is to make sure running pipelines keep working. The token rotation assumes the command `manage-service-account` is run as a scheduled task at least every hour. 

## TODO

- Design a global role model and implement that in this tool. Currently only one global role will vbe created.  
- Test if all preconditions are met to make the Rancher project operational. 
- Test the service account tokens