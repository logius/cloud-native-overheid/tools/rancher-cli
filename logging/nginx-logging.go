package logging

import (
	"bytes"
	"context"
	"log"
	"strings"
	"text/template"

	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/yaml"
)

func createNginxLoggingFlow(mc *rancher.MasterClient, k8sAPI *k8s.API, projectName string, clusterName string, outputname string, nginxNamespace string) error {

	project, err := GetRancherProject(mc, projectName, clusterName)
	if err != nil {
		return err
	}

	namespaces, err := GetExistingNamespaces(mc, project, clusterName)
	if err != nil {
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "logging.banzaicloud.io",
		Version:  "v1beta1",
		Resource: "flows",
	}

	type FlowParams struct {
		ClusterName   string
		FlowName      string
		Output        string
		NamespaceList string
	}
	flowParams := FlowParams{
		ClusterName:   clusterName,
		FlowName:      projectName + "-nginx-flow",
		Output:        outputname,
		NamespaceList: strings.Join(namespaces, "|"),
	}

	var flow unstructured.Unstructured
	fileName := "deployments/logging/nginx-flow.yml"
	err = parseDeploymentFile(fileName, &flowParams, &flow)
	if err != nil {
		return err
	}

	if existingFlow, err := k8sAPI.Dynamic.Resource(gvr).Namespace(nginxNamespace).Get(context.TODO(), flow.GetName(), metav1.GetOptions{}); err != nil {
		log.Printf("Create flow %q in namespace %s", flow.GetName(), nginxNamespace)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(nginxNamespace).Create(context.TODO(), &flow, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		flow.SetResourceVersion(existingFlow.GetResourceVersion())
		log.Printf("Update flow %q in namespace %s", flow.GetName(), nginxNamespace)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(nginxNamespace).Update(context.TODO(), &flow, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func parseDeploymentFile(fileName string, variables interface{}, object *unstructured.Unstructured) error {

	tpl, err := template.ParseFiles(fileName)
	if err != nil {
		tpl, err = template.ParseFiles("/" + fileName)
		if err != nil {
			return err
		}
	}

	var buf bytes.Buffer
	if err := tpl.Execute(&buf, variables); err != nil {
		return err
	}

	if err := yaml.Unmarshal(buf.Bytes(), object); err != nil {
		return err
	}

	return nil
}
