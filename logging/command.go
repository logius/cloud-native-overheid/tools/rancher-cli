package logging

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/rancher/norman/types"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

type flags struct {
	config           *string
	context          *string
	rancherURL       *string
	dataStreamSuffix *string
	token            *string
	nginxNamespace   *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureLogging(cmd, &flags)
		},
		Use:   "configure-nginx-logging",
		Short: "Configure Nginx Logging Flow",
		Long:  "This command configures logging Flow and Outputs for Nginx in k8s.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.context = cmd.Flags().String("context", "", "K8s context")
	flags.dataStreamSuffix = cmd.Flags().String("datastream-suffix", "", "Suffix for name of datastream")
	flags.nginxNamespace = cmd.Flags().String("nginx-namespace", "", "Namespace where Nginx is installed")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("dataStream-suffix")
	cmd.MarkFlagRequired("nginx-namespace")
	cmd.MarkFlagRequired("rancher-url")

	return cmd
}

func configureLogging(cmd *cobra.Command, flags *flags) error {

	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	k8sAPI := k8s.CreateAPI(*flags.context)

	for _, projectConfig := range customerConfig.Rancher.Projects {

		for _, clusterName := range projectConfig.Clusters {

			// only handle this project if there is a match between current k8s context and the project cluster
			if !strings.HasSuffix(clusterName, k8sAPI.Cluster) {
				continue
			}

			if customerConfig.ElasticTest != nil {
				log.Printf("Processing Nginx logging for ElasticTest. cluster=%q project=%q", clusterName, projectConfig.Name)
				if err := configureLoggingForElasticEnvironment(mc, k8sAPI, projectConfig.Name, customerConfig.ElasticTest, *flags.nginxNamespace, clusterName, *flags.dataStreamSuffix); err != nil {
					return err
				}
			}

			if customerConfig.ElasticProd != nil {
				log.Printf("Processing Nginx logging for ElasticProd. cluster=%q project=%q", clusterName, projectConfig.Name)
				if err := configureLoggingForElasticEnvironment(mc, k8sAPI, projectConfig.Name, customerConfig.ElasticProd, *flags.nginxNamespace, clusterName, *flags.dataStreamSuffix); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func configureLoggingForElasticEnvironment(mc *rancher.MasterClient, k8sAPI *k8s.API, projectName string, elasticSearchConfig *config.Elastic, nginxNamespace string, clusterName string, dataStreamSuffix string) error {
	datastream := getDataStream(elasticSearchConfig, clusterName, projectName)

	if datastream == "" {
		log.Printf("Skipping Nginx logging configuration for project %q, because cluster %q has no datastream associated", projectName, clusterName)
		return nil
	}

	outputname := fmt.Sprintf("%s%s", datastream, dataStreamSuffix)

	if !loggingOutputExists(k8sAPI, outputname) {
		return fmt.Errorf("Nginx flow could not be created because clusteroutput %q does not exist", outputname)
	}
	if err := createNginxLoggingFlow(mc, k8sAPI, projectName, clusterName, outputname, nginxNamespace); err != nil {
		return err
	}

	return nil
}

func loggingOutputExists(k8sAPI *k8s.API, outputname string) bool {

	gvr := schema.GroupVersionResource{
		Group:    "logging.banzaicloud.io",
		Version:  "v1beta1",
		Resource: "clusteroutputs",
	}

	_, err := k8sAPI.Dynamic.Resource(gvr).Namespace("l12m-logging").Get(context.TODO(), outputname, metav1.GetOptions{})
	return err == nil
}

func getDataStream(elasticSearchConfig *config.Elastic, clusterName string, projectName string) string {

	for _, ds := range elasticSearchConfig.DataStreams {
		if ds.Nginx.RancherProject == projectName {
			for _, requestedClusterName := range ds.Nginx.Clusters {
				if requestedClusterName == clusterName {
					return ds.Name
				}
			}
		}
	}
	return ""
}

func GetExistingNamespaces(mc *rancher.MasterClient, project *managementClient.Project, clusterName string) ([]string, error) {
	cc, err := mc.NewClusterClient(clusterName, project.ClusterID)
	if err != nil {
		return nil, err
	}

	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"projectId": project.ID,
		},
	}

	namespaces := make([]string, 0)
	nsCollection, err := cc.Namespace.List(&opts)
	if err != nil {
		return nil, err
	}
	for _, ns := range nsCollection.Data {
		namespaces = append(namespaces, ns.Name)
	}
	return namespaces, nil
}

func GetRancherProject(mc *rancher.MasterClient, projectName string, clusterName string) (*managementClient.Project, error) {

	clusterID, err := mc.GetClusterID(clusterName)
	if err != nil {
		return nil, err
	}
	filters := map[string]interface{}{
		"name":      projectName,
		"clusterId": clusterID,
	}
	projectCollection, err := mc.ManagementClient.Project.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return nil, err
	}

	if len(projectCollection.Data) > 0 {
		return &projectCollection.Data[0], nil
	}

	return nil, fmt.Errorf("could not find a Rancher project for %q in cluster %q", projectName, clusterName)
}
