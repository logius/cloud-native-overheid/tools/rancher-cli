package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/cisbenchmark"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/logging"
	logging_soc "gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/logging-soc"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/networkpolicies"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/project"
	rbac "gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rbac"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/reports"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/serviceaccount"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "rancher",
		Short: "OPS tools for Rancher",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(project.NewCommand())
	cmd.AddCommand(rbac.NewCommand())
	cmd.AddCommand(serviceaccount.NewCommandServiceAccount())
	cmd.AddCommand(serviceaccount.NewCommandAdminServiceAccount())
	cmd.AddCommand(serviceaccount.NewCommandAggregatedK8sConfig())

	cmd.AddCommand(reports.NewCommandStorageReport())
	cmd.AddCommand(reports.NewCommandResourceQuotaReport())
	cmd.AddCommand(cisbenchmark.NewCommand())
	cmd.AddCommand(logging.NewCommand())
	cmd.AddCommand(logging_soc.NewCommand())
	cmd.AddCommand(networkpolicies.NewCommand())
}
