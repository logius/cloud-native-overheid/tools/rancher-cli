package reports

import (
	"fmt"
	"sort"

	"github.com/xuri/excelize/v2"
	"k8s.io/apimachinery/pkg/api/resource"
)

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const redUsageThreshold = 0.2    // lower than 20% usage will be shown as red
const orangeUsageThreshold = 0.4 // lower than 40% usage will be shown as orange

type ExcelWriter struct {
	file                   *excelize.File
	sheet                  string //current sheet to write
	row                    int    // current row to write
	col                    int    // current col to write
	veryLowBackgroundStyle int
	lowBackgroundStyle     int
}

func (writer *ExcelWriter) nextColumn() string {
	col := writer.col
	writer.col++
	return fmt.Sprintf("%c%d", alphabet[col], writer.row)
}

func newExcelWriter() *ExcelWriter {
	var err error
	writer := &ExcelWriter{
		file: excelize.NewFile(),
	}

	writer.veryLowBackgroundStyle, err = writer.file.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Pattern: 1,
			Color:   []string{"#11EEEE"},
		},
	})
	if err != nil {
		fmt.Printf("%v", err)
	}
	writer.lowBackgroundStyle, err = writer.file.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Pattern: 1,
			Color:   []string{"#FFA500"},
		},
	})
	if err != nil {
		fmt.Printf("%v", err)
	}

	return writer
}

func createResourceQuotaReportFile(reportFile string, allRecords *[]resourceQuotaRecord, clusterQuantities map[string]*ResourceLimits) error {

	writer := newExcelWriter()

	defer writer.file.Close()

	if err := createCustomerSummarySheet(writer, allRecords); err != nil {
		return err
	}
	if err := createProjectsSheet(writer, allRecords); err != nil {
		return err
	}
	if err := createClusterSheet(writer, allRecords, clusterQuantities); err != nil {
		return err
	}
	if err := createAllDetailsSheet(writer, allRecords); err != nil {
		return err
	}

	writer.file.DeleteSheet("Sheet1")

	if err := writer.file.SaveAs(reportFile); err != nil {
		return err
	}

	return nil
}

func createCustomerSummarySheet(writer *ExcelWriter, allRecords *[]resourceQuotaRecord) error {

	writer.sheet = "Customers"
	indx, err := writer.file.NewSheet(writer.sheet)
	if err != nil {
		return err
	}
	writer.file.SetActiveSheet(indx)

	customerRecords := make(map[string]*resourceQuotaRecord, 0)

	for _, record := range *allRecords {
		customerRecord := customerRecords[record.customer]
		if customerRecord == nil {
			customerRecord = &resourceQuotaRecord{
				projectLimits:  &ResourceLimits{},
				appsUsedLimits: &ResourceLimits{},
			}
			customerRecords[record.customer] = customerRecord
		}
		customerRecord.projectLimits.add(*record.projectLimits)
		customerRecord.appsUsedLimits.add(*record.appsUsedLimits)
	}

	boldStyle, _ := writer.file.NewStyle(&excelize.Style{Font: &excelize.Font{Bold: true}})
	for indx, field := range []string{"Customer",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage"} {

		cell := fmt.Sprintf("%c1", alphabet[indx])
		writer.file.SetCellValue(writer.sheet, cell, field)

		column := fmt.Sprintf("%c", alphabet[indx])
		if indx == 0 {
			writer.file.SetColWidth(writer.sheet, column, column, 30)
		} else {
			writer.file.SetColWidth(writer.sheet, column, column, 15)
		}
		writer.file.SetCellStyle(writer.sheet, cell, cell, boldStyle)
	}

	keys := make([]string, 0, len(customerRecords))
	for k := range customerRecords {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	writer.row = 1
	for _, key := range keys {

		r := customerRecords[key]
		writer.col = 0
		writer.row++

		cell := writer.nextColumn()
		writer.file.SetCellStr(writer.sheet, cell, key)

		// CPU Requests
		if err := printCPULimits(writer, r.projectLimits.RequestsCPU, r.appsUsedLimits.RequestsCPU); err != nil {
			return nil
		}
		// CPU Limits
		if err := printCPULimits(writer, r.projectLimits.LimitsCPU, r.appsUsedLimits.LimitsCPU); err != nil {
			return nil
		}
		// Memory Requests
		if err := printGigaByteLimits(writer, r.projectLimits.RequestsMemory, r.appsUsedLimits.RequestsMemory); err != nil {
			return nil
		}
		// Memory Limits
		if err := printGigaByteLimits(writer, r.projectLimits.LimitsMemory, r.appsUsedLimits.LimitsMemory); err != nil {
			return nil
		}
		if err := printGigaByteLimits(writer, r.projectLimits.RequestsStorage, r.appsUsedLimits.RequestsStorage); err != nil {
			return nil
		}
	}

	createLegend(writer)

	return nil
}

func createProjectsSheet(writer *ExcelWriter, allRecords *[]resourceQuotaRecord) error {

	writer.sheet = "Projects"
	_, err := writer.file.NewSheet(writer.sheet)
	if err != nil {
		return err
	}

	for indx, field := range []string{
		"Customer", "Project", "Cluster",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage"} {
		writer.file.SetCellValue(writer.sheet, fmt.Sprintf("%c1", alphabet[indx]), field)

		column := fmt.Sprintf("%c", alphabet[indx])
		if indx == 2 {
			writer.file.SetColWidth(writer.sheet, column, column, 30)
		} else {
			writer.file.SetColWidth(writer.sheet, column, column, 15)
		}
	}

	boldStyle, _ := writer.file.NewStyle(&excelize.Style{Font: &excelize.Font{Bold: true}})
	writer.file.SetCellStyle(writer.sheet, "A1", "Z1", boldStyle)

	for row, r := range *allRecords {

		writer.row = row + 2
		writer.col = 0

		cell := writer.nextColumn()
		writer.file.SetCellStr(writer.sheet, cell, r.customer)
		cell = writer.nextColumn()
		writer.file.SetCellStr(writer.sheet, cell, r.project)
		cell = writer.nextColumn()
		writer.file.SetCellStr(writer.sheet, cell, r.cluster)

		if err := printCPULimits(writer, r.projectLimits.RequestsCPU, r.appsUsedLimits.RequestsCPU); err != nil {
			return nil
		}
		if err := printCPULimits(writer, r.projectLimits.LimitsCPU, r.appsUsedLimits.LimitsCPU); err != nil {
			return nil
		}
		if err := printGigaByteLimits(writer, r.projectLimits.RequestsMemory, r.appsUsedLimits.RequestsMemory); err != nil {
			return nil
		}
		if err := printGigaByteLimits(writer, r.projectLimits.LimitsMemory, r.appsUsedLimits.LimitsMemory); err != nil {
			return nil
		}
		if err := printGigaByteLimits(writer, r.projectLimits.RequestsStorage, r.appsUsedLimits.RequestsStorage); err != nil {
			return nil
		}
	}

	createLegend(writer)

	return nil
}

func createClusterSheet(writer *ExcelWriter, allRecords *[]resourceQuotaRecord, clusterQuantities map[string]*ResourceLimits) error {
	writer.sheet = "Clusters"
	_, err := writer.file.NewSheet(writer.sheet)
	if err != nil {
		return err
	}

	clusterRecords := make(map[string]*resourceQuotaRecord, 0)

	for _, record := range *allRecords {
		clusterRecord := clusterRecords[record.cluster]
		if clusterRecord == nil {
			clusterRecord = &resourceQuotaRecord{
				projectLimits:  &ResourceLimits{},
				appsUsedLimits: &ResourceLimits{},
			}
			clusterRecords[record.cluster] = clusterRecord
		}
		clusterRecord.projectLimits.add(*record.projectLimits)
		clusterRecord.appsUsedLimits.add(*record.appsUsedLimits)
	}

	// first header row
	writer.file.SetCellStr(writer.sheet, "B1", "Project allocatie")
	writer.file.MergeCell(writer.sheet, "B1", "F1")
	writer.file.SetCellStr(writer.sheet, "G1", "Apps allocatie")
	writer.file.MergeCell(writer.sheet, "G1", "K1")
	writer.file.SetCellStr(writer.sheet, "L1", "Cluster capaciteit")
	writer.file.MergeCell(writer.sheet, "L1", "Q1")

	// second header row
	writer.row = 2
	writer.col = 0

	for indx, field := range []string{"Cluster",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage",
		"CPU Cap", "Mem Cap"} {

		cell := writer.nextColumn()
		writer.file.SetCellValue(writer.sheet, cell, field)

		column := fmt.Sprintf("%c", alphabet[indx])
		if indx == 0 {
			writer.file.SetColWidth(writer.sheet, column, column, 30)
		} else {
			writer.file.SetColWidth(writer.sheet, column, column, 10)
		}
	}

	boldStyle, _ := writer.file.NewStyle(&excelize.Style{Font: &excelize.Font{Bold: true}})
	writer.file.SetCellStyle(writer.sheet, "A1", "Z2", boldStyle)

	keys := make([]string, 0, len(clusterRecords))
	for k := range clusterRecords {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, key := range keys {

		r := clusterRecords[key]
		writer.col = 0
		writer.row++

		cell := writer.nextColumn()
		writer.file.SetCellStr(writer.sheet, cell, key)

		if err := printLimits(writer, r.projectLimits); err != nil {
			return nil
		}
		if err := printLimits(writer, r.appsUsedLimits); err != nil {
			return nil
		}

		appsRequestsCPU := float64(r.appsUsedLimits.RequestsCPU.MilliValue()) / 1000
		clusterLimitsCPU := float64(clusterQuantities[key].LimitsCPU.MilliValue()) / 1000
		cell = writer.nextColumn()
		if err := writer.file.SetCellFloat(writer.sheet, cell, clusterLimitsCPU, 2, 64); err != nil {
			return err
		}
		setCellUsageColor(writer, cell, appsRequestsCPU, clusterLimitsCPU)

		appsRequestsMemory := r.appsUsedLimits.RequestsMemory.Value() / (1024 * 1024 * 1024)
		clusterLimitsMemory := clusterQuantities[key].LimitsMemory.Value() / (1024 * 1024 * 1024)
		cell = writer.nextColumn()
		if err := writer.file.SetCellInt(writer.sheet, cell, int(clusterLimitsMemory)); err != nil {
			return err
		}
		setCellUsageColor(writer, cell, float64(appsRequestsMemory), float64(clusterLimitsMemory))
	}

	writer.row++
	writer.col = 1

	summaryStyle, _ := writer.file.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
		Border: []excelize.Border{
			{
				Style: 5,
				Type:  "top",
			},
		}})

	for i := 1; i <= 12; i++ {
		cell := writer.nextColumn()
		formula := fmt.Sprintf("=SUM(%c3:%c%d)", alphabet[i], alphabet[i], len(keys)+2)
		writer.file.SetCellFormula(writer.sheet, cell, formula)
		writer.file.SetCellStyle(writer.sheet, cell, cell, summaryStyle)
	}

	createLegend(writer)

	return nil
}

func createAllDetailsSheet(writer *ExcelWriter, allRecords *[]resourceQuotaRecord) error {

	writer.sheet = "Details"
	_, err := writer.file.NewSheet(writer.sheet)
	if err != nil {
		return err
	}

	// first header row
	writer.file.SetCellStr(writer.sheet, "D1", "Project")
	writer.file.MergeCell(writer.sheet, "D1", "H1")
	writer.file.SetCellStr(writer.sheet, "I1", "Namespace")
	writer.file.MergeCell(writer.sheet, "I1", "M1")
	writer.file.SetCellStr(writer.sheet, "N1", "Apps")
	writer.file.MergeCell(writer.sheet, "N1", "R1")

	// second header row
	writer.row = 2
	writer.col = 0
	for indx, field := range []string{
		"Customer", "Project", "Cluster",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage",
		"CPU Req", "CPU Lim", "Mem Req", "Mem Lim", "Storage"} {

		cell := writer.nextColumn()
		writer.file.SetCellValue(writer.sheet, cell, field)

		column := fmt.Sprintf("%c", alphabet[indx])
		if indx == 2 {
			writer.file.SetColWidth(writer.sheet, column, column, 30)
		} else {
			writer.file.SetColWidth(writer.sheet, column, column, 10)
		}
	}

	boldStyle, _ := writer.file.NewStyle(&excelize.Style{Font: &excelize.Font{Bold: true}})
	writer.file.SetCellStyle(writer.sheet, "A1", "Z2", boldStyle)

	for _, r := range *allRecords {

		writer.row++
		writer.col = 0

		writer.file.SetCellStr(writer.sheet, fmt.Sprintf("%c%d", alphabet[writer.col], writer.row), r.customer)
		writer.col++
		writer.file.SetCellStr(writer.sheet, fmt.Sprintf("%c%d", alphabet[writer.col], writer.row), r.project)
		writer.col++
		writer.file.SetCellStr(writer.sheet, fmt.Sprintf("%c%d", alphabet[writer.col], writer.row), r.cluster)
		writer.col++

		if err := printLimits(writer, r.projectLimits); err != nil {
			return nil
		}
		if err := printLimits(writer, r.projectUsedLimits); err != nil {
			return nil
		}
		if err := printLimits(writer, r.appsUsedLimits); err != nil {
			return nil
		}
	}
	return nil
}

func printGigaByteLimits(writer *ExcelWriter, projectQuantity resource.Quantity, appsQuantity resource.Quantity) error {

	projectlimits := projectQuantity.Value() / (1024 * 1024 * 1024)
	appsLimits := appsQuantity.Value() / (1024 * 1024 * 1024)

	value := fmt.Sprintf("%d van %d", appsLimits, projectlimits)
	cell := writer.nextColumn()
	if err := writer.file.SetCellStr(writer.sheet, cell, value); err != nil {
		return err
	}
	setCellUsageColor(writer, cell, float64(appsLimits), float64(projectlimits))

	return nil
}

func printCPULimits(writer *ExcelWriter, projectQuantity resource.Quantity, appsQuantity resource.Quantity) error {

	limitsCPU := float64(projectQuantity.MilliValue()) / 1000
	appsLimitsCPU := float64(appsQuantity.MilliValue()) / 1000

	cell := writer.nextColumn()
	value := fmt.Sprintf("%.0f van %.0f", appsLimitsCPU, limitsCPU)
	if err := writer.file.SetCellStr(writer.sheet, cell, value); err != nil {
		return err
	}
	setCellUsageColor(writer, cell, appsLimitsCPU, limitsCPU)

	return nil
}

func printLimits(writer *ExcelWriter, limits *ResourceLimits) error {

	requestsCPU := float64(limits.RequestsCPU.MilliValue()) / 1000
	cell := writer.nextColumn()
	if err := writer.file.SetCellFloat(writer.sheet, cell, requestsCPU, 1, 64); err != nil {
		return err
	}

	limitsCPU := float64(limits.LimitsCPU.MilliValue()) / 1000
	cell = writer.nextColumn()
	if err := writer.file.SetCellFloat(writer.sheet, cell, limitsCPU, 1, 64); err != nil {
		return err
	}

	requestsMemory := limits.RequestsMemory.Value() / (1024 * 1024 * 1024)
	cell = writer.nextColumn()
	if err := writer.file.SetCellInt(writer.sheet, cell, int(requestsMemory)); err != nil {
		return err
	}

	limitsMemory := limits.LimitsMemory.Value() / (1024 * 1024 * 1024)
	cell = writer.nextColumn()
	if err := writer.file.SetCellInt(writer.sheet, cell, int(limitsMemory)); err != nil {
		return err
	}

	requestsStorage := limits.RequestsStorage.Value() / (1024 * 1024 * 1024)
	cell = writer.nextColumn()
	if err := writer.file.SetCellInt(writer.sheet, cell, int(requestsStorage)); err != nil {
		return err
	}

	return nil
}

func createLegend(writer *ExcelWriter) {
	writer.row += 2
	writer.col = 1
	cell := writer.nextColumn()
	writer.file.SetCellStyle(writer.sheet, cell, cell, writer.veryLowBackgroundStyle)
	cell = writer.nextColumn()
	writer.file.SetCellStr(writer.sheet, cell, "Minder dan 40% utilisatie")

	writer.row++
	writer.col = 1
	cell = writer.nextColumn()
	writer.file.SetCellStyle(writer.sheet, cell, cell, writer.lowBackgroundStyle)
	cell = writer.nextColumn()
	writer.file.SetCellStr(writer.sheet, cell, "Minder dan 20% utilisatie")
}

func setCellUsageColor(writer *ExcelWriter, cell string, request float64, limit float64) {
	if request/limit < redUsageThreshold {
		writer.file.SetCellStyle(writer.sheet, cell, cell, writer.veryLowBackgroundStyle)
	} else if request/limit < orangeUsageThreshold {
		writer.file.SetCellStyle(writer.sheet, cell, cell, writer.lowBackgroundStyle)
	}
}
