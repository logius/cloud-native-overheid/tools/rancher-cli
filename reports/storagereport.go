package reports

import (
	"fmt"

	"github.com/docker/go-units"
	"github.com/rancher/norman/types"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	projectClient "github.com/rancher/rancher/pkg/client/generated/project/v3"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
)

func generateStorageReport(cmd *cobra.Command, flags *flags) error {

	// Rancher API client
	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	projects, err := getRancherProjects(mc)
	if err != nil {
		return err
	}

	customerProjects := collectCustomerProjects(projects)

	totalBytesPerCluster := make(map[string]map[string]int64)

	for customerName, projects := range customerProjects {
		err := generateStorageReportForCustomer(mc, customerName, projects, totalBytesPerCluster)
		if err != nil {
			return err
		}
	}

	for clusterName, storageMap := range totalBytesPerCluster {
		for storageClass, pvcBytes := range storageMap {
			fmt.Printf("\ncluster %s %s %s\n", clusterName, storageClass, units.HumanSize(float64(pvcBytes)))
		}
	}
	return nil
}

func generateStorageReportForCustomer(mc *rancher.MasterClient, customerName string, projects []managementClient.Project, totalBytesPerCluster map[string]map[string]int64) error {
	totalBytesPerCustomer := make(map[string]int64)

	fmt.Printf("\n%s\n", customerName)
	for _, project := range projects {
		totalBytesPerCustomerProject := make(map[string]int64)
		fmt.Printf("  project %s\n", project.Name)

		pvcClaims, err := getPVCs(mc, &project)

		if err != nil {
			return err
		}

		if len(pvcClaims) > 0 {
			clusterName, err := mc.GetClusterName(project.ClusterID)
			if err != nil {
				return err
			}
			fmt.Printf("    cluster %s\n", clusterName)

			for _, pvcClaim := range pvcClaims {
				storage := pvcClaim.Resources.Requests["storage"]
				fmt.Printf("      %s (PVC %s in ns %s met storageclass %s)\n",
					storage,
					pvcClaim.Name,
					pvcClaim.NamespaceId,
					pvcClaim.StorageClassID)
				pvcBytes, err := units.FromHumanSize(storage)
				if err != nil {
					return fmt.Errorf("could not parse %q in pvcClaim %q in namespace %q. Error: %w", storage, pvcClaim.Name, pvcClaim.NamespaceId, err)
				}

				totalBytesPerCustomerProject[pvcClaim.StorageClassID] += pvcBytes
				totalBytesPerCustomer[pvcClaim.StorageClassID] += pvcBytes
				if totalBytesPerCluster[clusterName] == nil {
					totalBytesPerCluster[clusterName] = make(map[string]int64)
				}
				totalBytesPerCluster[clusterName][pvcClaim.StorageClassID] += pvcBytes
			}
		}
		for storageClass, pvcBytes := range totalBytesPerCustomerProject {
			fmt.Printf("    project total %s %s\n", storageClass, units.HumanSize(float64(pvcBytes)))
		}

	}
	for storageClass, pvcBytes := range totalBytesPerCustomer {
		fmt.Printf("  customer total %s %s\n", storageClass, units.HumanSize(float64(pvcBytes)))
	}
	return nil
}

func getPVCs(mc *rancher.MasterClient, project *managementClient.Project) ([]projectClient.PersistentVolumeClaim, error) {

	pc, err := mc.NewProjectClient(project.Name, project.ID)
	if err != nil {
		return nil, err
	}
	pvcCollection, err := pc.PersistentVolumeClaim.ListAll(&types.ListOpts{})

	return pvcCollection.Data, err
}
