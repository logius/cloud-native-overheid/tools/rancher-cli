package reports

import (
	"context"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/rancher/norman/types"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ResourceLimits struct {
	LimitsCPU       resource.Quantity
	LimitsMemory    resource.Quantity
	RequestsCPU     resource.Quantity
	RequestsMemory  resource.Quantity
	RequestsStorage resource.Quantity
}

func (r *ResourceLimits) add(r2 ResourceLimits) {
	r.LimitsCPU.Add(r2.LimitsCPU)
	r.RequestsCPU.Add(r2.RequestsCPU)
	r.LimitsMemory.Add(r2.LimitsMemory)
	r.RequestsMemory.Add(r2.RequestsMemory)
	r.RequestsStorage.Add(r2.RequestsStorage)
}

type resourceQuotaRecord struct {
	cluster           string
	customer          string
	project           string
	projectLimits     *ResourceLimits
	projectUsedLimits *ResourceLimits
	appsUsedLimits    *ResourceLimits
}

type ByCustomer []resourceQuotaRecord

func (a ByCustomer) Len() int      { return len(a) }
func (a ByCustomer) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByCustomer) Less(i, j int) bool {
	if a[i].customer != a[j].customer {
		return a[i].customer < a[j].customer
	}
	if a[i].project != a[j].project {
		return a[i].project < a[j].project
	}
	return a[i].cluster < a[j].cluster
}

type flagsQuotaReport struct {
	rancherURLAZ1 *string
	rancherURLAZ2 *string
	reportFile    *string
}

// NewCommand creates a new command
func NewCommandResourceQuotaReport() *cobra.Command {

	flags := flagsQuotaReport{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return generateResourceQuotaReport(cmd, &flags)
		},
		Use:   "generate-resourcequota-report",
		Short: "Generate a ResourceQuota report",
		Long:  "This command generates a report with ResourceQuota's per project.",
	}

	flags.rancherURLAZ1 = cmd.Flags().String("rancher-url-az1", "", "URL of Rancher AZ1")
	flags.rancherURLAZ2 = cmd.Flags().String("rancher-url-az2", "", "URL of Rancher AZ2")
	flags.reportFile = cmd.Flags().String("report-file", "/tmp/output/quotas.xlsx", "File path for report output")

	cmd.MarkFlagRequired("rancher-url")

	return cmd
}

func generateResourceQuotaReport(cmd *cobra.Command, flags *flagsQuotaReport) error {

	var allRecords []resourceQuotaRecord
	clusterQuantities := make(map[string]*ResourceLimits, 0)

	mcAZ1, err := rancher.NewMasterClient(*flags.rancherURLAZ1, os.Getenv("RANCHER_AZ1_TOKEN"))
	if err != nil {
		return err
	}
	mcAZ2, err := rancher.NewMasterClient(*flags.rancherURLAZ2, os.Getenv("RANCHER_AZ2_TOKEN"))
	if err != nil {
		return err
	}

	if err := collect(mcAZ1, &allRecords); err != nil {
		return err
	}
	if err := collect(mcAZ2, &allRecords); err != nil {
		return err
	}
	if err := collectClusterQuantities(mcAZ1, clusterQuantities); err != nil {
		return err
	}
	if err := collectClusterQuantities(mcAZ2, clusterQuantities); err != nil {
		return err
	}

	sort.Sort(ByCustomer(allRecords))

	reportPath := filepath.Dir(*flags.reportFile)

	if _, err := os.Stat(reportPath); err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(reportPath, 0755)
			if err != nil {
				return err
			}
		}
	}

	return createResourceQuotaReportFile(*flags.reportFile, &allRecords, clusterQuantities)
}

func collectClusterQuantities(mc *rancher.MasterClient, clusterQuantities map[string]*ResourceLimits) error {

	clusters, err := mc.ManagementClient.Cluster.ListAll(&types.ListOpts{})
	if err != nil {
		return err
	}

	for _, cluster := range clusters.Data {
		cpuQuantity, _ := resource.ParseQuantity(cluster.Capacity["cpu"])
		memQuantity, _ := resource.ParseQuantity(cluster.Capacity["memory"])
		clusterQuantities[cluster.Name] = &ResourceLimits{
			LimitsCPU:    cpuQuantity,
			LimitsMemory: memQuantity,
		}

	}
	return nil
}

func collect(mc *rancher.MasterClient, allRecords *[]resourceQuotaRecord) error {

	projects, err := getRancherProjects(mc)
	if err != nil {
		return err
	}

	customerProjects := collectCustomerProjects(projects)

	for customerName, projects := range customerProjects {
		err := addResourceQuota(mc, customerName, projects, allRecords)
		if err != nil {
			return err
		}
	}
	return nil
}

func convertContainerResources(resources corev1.ResourceRequirements) (*ResourceLimits, error) {
	resourceLimits := &ResourceLimits{}

	if resources.Limits.Cpu() != nil {
		resourceLimits.LimitsCPU = *resources.Limits.Cpu()
	}
	if resources.Limits.Memory() != nil {
		resourceLimits.LimitsMemory = *resources.Limits.Memory()
	}
	if resources.Requests.Cpu() != nil {
		resourceLimits.RequestsCPU = *resources.Requests.Cpu()
	}
	if resources.Requests.Memory() != nil {
		resourceLimits.RequestsMemory = *resources.Requests.Memory()
	}

	return resourceLimits, nil
}

func convertToResourceLimits(limit *managementClient.ResourceQuotaLimit) (*ResourceLimits, error) {
	resourceLimits := &ResourceLimits{}
	var err error
	if limit.LimitsCPU != "" {
		if resourceLimits.LimitsCPU, err = resource.ParseQuantity(limit.LimitsCPU); err != nil {
			return nil, err
		}
	}
	if limit.LimitsMemory != "" {
		if resourceLimits.LimitsMemory, err = resource.ParseQuantity(limit.LimitsMemory); err != nil {
			return nil, err
		}
	}
	if limit.RequestsCPU != "" {
		if resourceLimits.RequestsCPU, err = resource.ParseQuantity(limit.RequestsCPU); err != nil {
			return nil, err
		}
	}
	if limit.RequestsMemory != "" {
		if resourceLimits.RequestsMemory, err = resource.ParseQuantity(limit.RequestsMemory); err != nil {
			return nil, err
		}
	}
	if limit.RequestsStorage != "" {
		if resourceLimits.RequestsStorage, err = resource.ParseQuantity(limit.RequestsStorage); err != nil {
			return nil, err
		}
	}

	return resourceLimits, nil
}

func addResourceQuota(mc *rancher.MasterClient, customerName string, projects []managementClient.Project, allRecords *[]resourceQuotaRecord) error {
	for _, project := range projects {
		var err error

		clusterName, err := mc.GetClusterName(project.ClusterID)
		if err != nil {
			return err
		}
		limits := &ResourceLimits{}
		usedLimit := &ResourceLimits{}
		if project.ResourceQuota != nil {

			if project.ResourceQuota.Limit != nil {
				limits, err = convertToResourceLimits(project.ResourceQuota.Limit)
				if err != nil {
					return err
				}
			}
			if project.ResourceQuota.UsedLimit != nil {
				usedLimit, err = convertToResourceLimits(project.ResourceQuota.UsedLimit)
				if err != nil {
					return err
				}
			}
		}

		parts := strings.Split(clusterName, "-")
		shortClusterName := strings.Join(parts[3:], "-")
		k8sAPI := k8s.CreateAPI(shortClusterName)
		namespaces, err := getProjectNamespaces(mc, &project, clusterName)
		if err != nil {
			return err
		}

		appsLimits := &ResourceLimits{}
		for _, ns := range namespaces {
			quotas, err := k8sAPI.Clientset.CoreV1().ResourceQuotas(ns).List(context.TODO(), v1.ListOptions{})
			if err != nil {
				return err
			}
			for _, quota := range quotas.Items {
				appsLimits.RequestsCPU.Add(quota.Status.Used["requests.cpu"])
				appsLimits.LimitsCPU.Add(quota.Status.Used["limits.cpu"])
				appsLimits.RequestsMemory.Add(quota.Status.Used["requests.memory"])
				appsLimits.LimitsMemory.Add(quota.Status.Used["limits.memory"])
				appsLimits.RequestsStorage.Add(quota.Status.Used["requests.storage"])
			}

			if len(quotas.Items) == 0 {
				pods, err := k8sAPI.Clientset.CoreV1().Pods(ns).List(context.TODO(), v1.ListOptions{})
				if err != nil {
					return err
				}
				for _, pod := range pods.Items {
					for _, container := range pod.Spec.Containers {
						resourceLimits, _ := convertContainerResources(container.Resources)
						appsLimits.add(*resourceLimits)
					}
				}
			}
		}

		newRecord := resourceQuotaRecord{
			cluster:           clusterName,
			customer:          customerName,
			project:           project.Name,
			projectLimits:     limits,
			projectUsedLimits: usedLimit,
			appsUsedLimits:    appsLimits,
		}
		*allRecords = append(*allRecords, newRecord)
	}
	return nil
}

func getProjectNamespaces(mc *rancher.MasterClient, project *managementClient.Project, clusterName string) ([]string, error) {
	cc, err := mc.NewClusterClient(clusterName, project.ClusterID)
	if err != nil {
		return nil, err
	}

	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"projectId": project.ID,
		},
	}

	namespaces := make([]string, 0)
	nsCollection, err := cc.Namespace.List(&opts)
	if err != nil {
		return nil, err
	}
	for _, ns := range nsCollection.Data {
		namespaces = append(namespaces, ns.Name)
	}
	return namespaces, nil
}
