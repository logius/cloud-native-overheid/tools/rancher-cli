package reports

import (
	"os"

	"github.com/rancher/norman/types"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
)

type flags struct {
	rancherURL *string
	token      *string
}

// NewCommand creates a new command
func NewCommandStorageReport() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return generateStorageReport(cmd, &flags)
		},
		Use:   "generate-storage-report",
		Short: "Generate a storage report",
		Long:  "This command generates a report with allocated PVC storage per tenant.",
	}

	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")

	cmd.MarkFlagRequired("rancher-url")

	return cmd
}

func getRancherProjects(mc *rancher.MasterClient) ([]managementClient.Project, error) {

	projectCollection, err := mc.ManagementClient.Project.List(&types.ListOpts{})
	if err != nil {
		return nil, err
	}

	return projectCollection.Data, nil
}

func collectCustomerProjects(projects []managementClient.Project) map[string][]managementClient.Project {
	allCustomerProjects := make(map[string][]managementClient.Project)

	for _, project := range projects {
		for key, value := range project.Labels {
			if key == "customer" {
				customerProjects := append(allCustomerProjects[value], project)
				allCustomerProjects[value] = customerProjects
			}
		}
	}
	return allCustomerProjects
}
