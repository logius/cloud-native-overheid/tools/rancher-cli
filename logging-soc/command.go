package logging_soc

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"html/template"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/rancher/norman/types"
	client "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/yaml"
)

type flags struct {
	config        *string
	rancherURL    *string
	brokerUrlMap  map[string]string
	token         *string
	clusterfilter *[]string
}

type OutputParams struct {
	CustomerName    string
	CertificateName string
	Hash            string
	TopicName       string
	ProjectID       string // this may be jused to check if a Flow is allowed to connect to this ClusterOutput
	BrokerUrl       string
}

const namespace = "l12m-logging"

/*
1. A topic contains 1 or more rancher projects. A map is created that contains the rancher projects per cluster for a specific topic
2. Loop through the clusters (containing 1 or more rancher projects)
3. For each topic (in all relevant clusters), a certmanager certificate request and a clusteroutput is deployed in the namespace l12m-logging
4. The clusteroutput is annotated with the Rancher project id's configured for the topic
*/
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureSocLogging(cmd, &flags)
		},
		Use:   "configure-soc-logging",
		Short: "Configure Soc Logging ",
		Long:  "This command configures soc logging.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")
	cmd.Flags().StringToStringVar(&flags.brokerUrlMap, "broker-url", nil, "Array of nv pairs with environment-az/url")
	flags.clusterfilter = cmd.Flags().StringSlice("cluster-filter", []string{}, "rancher cluster name prefix filter")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("broker-url")
	cmd.MarkFlagRequired("rancher-url")

	return cmd
}

func configureSocLogging(cmd *cobra.Command, flags *flags) error {

	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	sortedBrokerUrlMapKeys := createSortedUrlmap(flags.brokerUrlMap)
	customerName := strings.ToLower(customerConfig.Name)
	createResourcesMap := make(map[string][]OutputParams)
	for _, soc_logconfig := range customerConfig.SocLogging {

		topicName := strings.ToLower(soc_logconfig.Topic)
		//topicName has to start with lpc_<klantnaam>
		if !strings.HasPrefix(topicName, fmt.Sprintf("lpc_%s", customerName)) {
			if strings.HasPrefix(topicName, "lpc_") {
				return fmt.Errorf("Invalid topic name: %s", topicName)
			}
			topicName = fmt.Sprintf("lpc_%s_%s", customerName, strings.ToLower(soc_logconfig.Topic))
		}
		projectsMap := make(map[string][]*client.Project)
		for _, rancherProject := range soc_logconfig.RancherProjects {
			filters := map[string]interface{}{
				"name": rancherProject,
			}

			projectCollection, err := mc.ManagementClient.Project.List(&types.ListOpts{Filters: filters})
			if err != nil {
				return err
			}

			for _, project := range projectCollection.Data {

				cluster, err := mc.ManagementClient.Cluster.ByID(project.ClusterID)
				if err != nil {
					return err
				}

				if !strings.EqualFold(project.Labels["customer"], customerConfig.Name) {
					log.Printf("Customer %s not found in labels for project %s on cluster %s. Skipping", customerConfig.Name, project.Name, cluster.Name)
					continue
				}

				projectsMap[cluster.Name] = append(projectsMap[cluster.Name], &project)

			}
		}

		err = prepareSocLoggingForProjectsInCluster(topicName, customerName, &projectsMap, &sortedBrokerUrlMapKeys, flags, createResourcesMap)
		if err != nil {
			return err
		}

	}

	err = createSocLoggingForProjectsInCluster(mc, customerName, createResourcesMap, flags)
	if err != nil {
		return err
	}

	return err
}

func prepareSocLoggingForProjectsInCluster(topic string, customerName string, projectsMap *map[string][]*client.Project, sortedBrokerUrlMapKeys *[]string, flags *flags, createResources map[string][]OutputParams) error {

	log.Printf("Topic=%s, found number of clusters %d", topic, len(*projectsMap))
	for clusterName, projectList := range *projectsMap {

		projectIdList := make([]string, 0)
		projectNameList := make([]string, 0)
		for _, project := range projectList {
			projectIdList = append(projectIdList, project.ID)
			projectNameList = append(projectNameList, project.Name)
		}
		projectIds := strings.Join(projectIdList, ",")
		projectNames := strings.Join(projectNameList, ",")

		for _, clusterSuffix := range *sortedBrokerUrlMapKeys {
			if strings.HasSuffix(clusterName, clusterSuffix) {
				log.Printf("cluster %q/%q(%s) brokerurl=%q", clusterName, projectIds, projectNames, flags.brokerUrlMap[clusterSuffix])

				outputParams := OutputParams{
					CustomerName:    customerName,
					CertificateName: strings.ReplaceAll(string(topic), "_", "-"),
					TopicName:       topic,
					ProjectID:       projectIds,
					BrokerUrl:       flags.brokerUrlMap[clusterSuffix],
				}
				createResources[clusterName] = append(createResources[clusterName], outputParams)
				break
			}
		}
	}
	return nil
}

// creates all objects cluster by cluster
func createSocLoggingForProjectsInCluster(mc *rancher.MasterClient, customerName string, createResources map[string][]OutputParams, flags *flags) error {

	allclusters, err := mc.ManagementClient.Cluster.ListAll(&types.ListOpts{})
	if err != nil {
		return err
	}
	for _, cluster := range allclusters.Data {

		found := true
		for _, clusterPrefix := range *flags.clusterfilter {
			found = false
			if strings.HasPrefix(cluster.Name, clusterPrefix) {
				found = true
				break
			}
		}
		if !found {
			continue
		}

		parts := strings.Split(cluster.Name, "-")
		shortClusterName := strings.Join(parts[3:], "-")

		k8sAPI := k8s.CreateAPI(shortClusterName)
		createdResources := make(map[string]bool)
		for createInclusterName, outputParamsArray := range createResources {

			if createInclusterName == cluster.Name {
				for _, outputParams := range outputParamsArray {

					createdResources[outputParams.CertificateName] = true
					err := createCertificate(k8sAPI, outputParams)
					if err != nil {
						return err
					}

					hash, err := getsecretHash(k8sAPI, outputParams)
					if err != nil {
						return err
					}
					outputParams.Hash = hash

					err = createClusterOutput(k8sAPI, outputParams)
					if err != nil {
						return err
					}
				}
				break
			}
		}
		cleanupResources(k8sAPI, customerName, createdResources)
	}

	return nil
}

func cleanupResources(k8sAPI *k8s.API, customerName string, createdResources map[string]bool) error {
	gvrCertificates := schema.GroupVersionResource{
		Group:    "cert-manager.io",
		Version:  "v1",
		Resource: "certificates",
	}
	gvrClusteroutputs := schema.GroupVersionResource{
		Group:    "logging.banzaicloud.io",
		Version:  "v1beta1",
		Resource: "clusteroutputs",
	}

	foundCertificates, err := k8sAPI.Dynamic.Resource(gvrCertificates).Namespace(namespace).List(context.TODO(), metav1.ListOptions{LabelSelector: fmt.Sprintf("lpc.customername=%s", customerName)})
	if (err != nil && !errors.IsNotFound(err)) || foundCertificates == nil {
		return err
	}
	for _, resource := range foundCertificates.Items {
		if !createdResources[resource.GetName()] {
			log.Printf("Delete cert %s", resource.GetName())
			k8sAPI.Dynamic.Resource(gvrCertificates).Namespace(namespace).Delete(context.TODO(), resource.GetName(), metav1.DeleteOptions{})

		}
	}
	foundClusteroutputs, err := k8sAPI.Dynamic.Resource(gvrClusteroutputs).Namespace(namespace).List(context.TODO(), metav1.ListOptions{LabelSelector: fmt.Sprintf("lpc.customername=%s", customerName)})
	if (err != nil && !errors.IsNotFound(err)) || foundClusteroutputs == nil {
		return err
	}
	for _, resource := range foundClusteroutputs.Items {
		if !createdResources[resource.GetName()] {
			log.Printf("Delete Clusteroutput %s", resource.GetName())
			k8sAPI.Dynamic.Resource(gvrClusteroutputs).Namespace(namespace).Delete(context.TODO(), resource.GetName(), metav1.DeleteOptions{})
		}
	}
	foundsecrets, err := k8sAPI.Clientset.CoreV1().Secrets(namespace).List(context.TODO(), metav1.ListOptions{LabelSelector: fmt.Sprintf("lpc.customername=%s", customerName)})
	if (err != nil && !errors.IsNotFound(err)) || foundsecrets == nil {
		return err
	}
	for _, resource := range foundsecrets.Items {
		if !createdResources[resource.GetName()] {
			log.Printf("Delete secret %s", resource.GetName())
			k8sAPI.Clientset.CoreV1().Secrets(namespace).Delete(context.TODO(), resource.GetName(), metav1.DeleteOptions{})
		}
	}
	return nil
}

func getsecretHash(k8sAPI *k8s.API, outputParams OutputParams) (string, error) {

	var existingSecret *v1.Secret
	var err error
	for i := 0; i < 10; i++ {
		existingSecret, err = k8sAPI.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), outputParams.CertificateName, metav1.GetOptions{})
		if errors.IsNotFound(err) {
			time.Sleep(2000 * time.Millisecond)
			continue
		} else if err != nil {
			return "", err
		}
	}

	if existingSecret == nil {
		return "", fmt.Errorf("Secret %s not ready after 20 seconds", outputParams.CertificateName)
	}

	h := sha256.New()
	_, err = h.Write([]byte(existingSecret.Data["tls.crt"]))
	hash := h.Sum(nil)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString([]byte(hash)), nil
}

func createCertificate(k8sAPI *k8s.API, outputParams OutputParams) error {

	var certificate unstructured.Unstructured
	fileName := "deployments/logging-soc/certificate.yaml"
	if err := parseDeploymentFile(fileName, &outputParams, &certificate); err != nil {
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "cert-manager.io",
		Version:  "v1",
		Resource: "certificates",
	}

	if existingCertificate, err := k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Get(context.TODO(), certificate.GetName(), metav1.GetOptions{}); err != nil {
		log.Printf("Create Certificate %q in namespace %s", certificate.GetName(), namespace)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Create(context.TODO(), &certificate, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		certificate.SetResourceVersion(existingCertificate.GetResourceVersion())
		log.Printf("Update Certificate %q in namespace %s", certificate.GetName(), namespace)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Update(context.TODO(), &certificate, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}

	return nil
}

func createClusterOutput(k8sAPI *k8s.API, outputParams OutputParams) error {

	var output unstructured.Unstructured
	fileName := "deployments/logging-soc/cluster-output.yaml"
	if err := parseDeploymentFile(fileName, &outputParams, &output); err != nil {
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "logging.banzaicloud.io",
		Version:  "v1beta1",
		Resource: "clusteroutputs",
	}

	if existingClusteroutput, err := k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Get(context.TODO(), output.GetName(), metav1.GetOptions{}); err != nil {
		log.Printf("Create clusteroutput %q in namespace %s", output.GetName(), namespace)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Create(context.TODO(), &output, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		output.SetResourceVersion(existingClusteroutput.GetResourceVersion())
		log.Printf("Update clusteroutput %q in namespace %s", output.GetName(), namespace)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Update(context.TODO(), &output, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}

	return nil
}

func parseDeploymentFile(fileName string, variables interface{}, object *unstructured.Unstructured) error {

	tpl, err := template.ParseFiles(fileName)
	if err != nil {
		tpl, err = template.ParseFiles("/" + fileName)
		if err != nil {
			return err
		}
	}

	var buf bytes.Buffer
	if err := tpl.Execute(&buf, variables); err != nil {
		return err
	}

	if err := yaml.Unmarshal(buf.Bytes(), object); err != nil {
		return err
	}

	return nil
}

// sorts map on string length of keys
func createSortedUrlmap(brokerUrlMap map[string]string) []string {
	sortedBrokerUrlMapKeys := make([]string, 0, len(brokerUrlMap))
	for key := range brokerUrlMap {
		sortedBrokerUrlMapKeys = append(sortedBrokerUrlMapKeys, key)
	}

	sort.Slice(sortedBrokerUrlMapKeys, func(i, j int) bool {
		return len(sortedBrokerUrlMapKeys[i]) > len(sortedBrokerUrlMapKeys[j])
	})

	return sortedBrokerUrlMapKeys
}
