package project

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
)

type flags struct {
	config                      *string
	rancherURL                  *string
	token                       *string
	groupFormat                 *string
	allowedClusterRoleCustomers *[]string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureProject(cmd, &flags)
		},
		Use:   "configure-project",
		Short: "Configure Rancher project",
		Long:  "This command configures a project in Rancher.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.groupFormat = cmd.Flags().String("group-format", "%s", "Format for full LDAP groupname, e.g. activedirectory_group://CN=%s,OU=Groups,DC=company,DC=io")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")
	flags.allowedClusterRoleCustomers = cmd.Flags().StringSlice("allowed-clusterrole-customers", []string{}, "List of customers that are allowed to configure clusterroles")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("rancher-url")

	return cmd
}

func configureProject(cmd *cobra.Command, flags *flags) error {

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	err = manageClusterroleBindings(mc, customerConfig, flags.allowedClusterRoleCustomers, *flags.groupFormat)
	if err != nil {
		return err
	}
	return manageProjectsInClusters(mc, customerConfig, *flags.groupFormat)
}
