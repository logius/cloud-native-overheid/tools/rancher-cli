package project

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/rancher/norman/clientbase"
	"github.com/rancher/norman/types"
	clusterClient "github.com/rancher/rancher/pkg/client/generated/cluster/v3"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
)

func manageProjectsInClusters(mc *rancher.MasterClient, customerConfig *config.Customer, groupFormat string) error {

	for _, projectConfig := range customerConfig.Rancher.Projects {

		for _, cluster := range projectConfig.Clusters {
			clusterID, err := mc.GetClusterID(cluster)
			if err != nil {
				log.Printf("Skip cluster %q as it could not be found in this Rancher", cluster)
				continue
			}

			project, err := manageProject(mc, clusterID, &projectConfig, customerConfig.Name)
			if err != nil {
				return err
			}

			for i := 0; i < 5; i++ {
				err = manageRolebindings(mc, cluster, clusterID, project, &projectConfig, groupFormat)
				if err == nil {
					break
				}
				if i < 4 {
					// it might be a new project that is not ready yet. wait a little and try again.
					time.Sleep(time.Second)
					continue
				}
				return err
			}

			cc, err := mc.NewClusterClient(cluster, clusterID)
			if err != nil {
				return err
			}

			if !projectConfig.DisableResourceQuota {
				for _, ns := range projectConfig.Namespaces {

					err := manageNamespace(cc, project, &projectConfig, ns)
					if err != nil {
						return err
					}
				}
			}

			if !projectConfig.DisableServicesNamespace {
				if err = createServiceNamespace(cc, project, &projectConfig); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func createServiceNamespace(cc *clusterClient.Client, project *managementClient.Project, projectConfig *config.Project) error {

	namespace := strings.ToLower(projectConfig.Name) + "-services"
	filters := map[string]interface{}{
		"name":      namespace,
		"projectId": project.ID,
	}
	namespaces, err := cc.Namespace.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return err
	}
	if len(namespaces.Data) > 0 {
		return nil // namespace already exists
	}

	ns := clusterClient.Namespace{
		Name:      namespace,
		ProjectID: project.ID,
		ResourceQuota: &clusterClient.NamespaceResourceQuota{
			Limit: &clusterClient.ResourceQuotaLimit{
				RequestsCPU:     "0",
				RequestsMemory:  "0",
				RequestsStorage: "0",
				LimitsCPU:       "0",
				LimitsMemory:    "0",
			},
		},
		Labels: map[string]string{
			"rbacmanager": "services", // used for rbac-manager to distinguish between the services namespace and customer namespaces
		},
	}

	log.Printf("Create namespace %s", ns.Name)
	_, err = cc.Namespace.Create(&ns)
	return err
}

func manageNamespace(cc *clusterClient.Client, project *managementClient.Project, projectConfig *config.Project, nsConfig config.Namespace) error {

	filters := map[string]interface{}{
		"name":      nsConfig.Name,
		"projectId": project.ID,
	}
	namespaces, err := cc.Namespace.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return err
	}

	ns := clusterClient.Namespace{
		Name:      nsConfig.Name,
		ProjectID: project.ID,
		ResourceQuota: &clusterClient.NamespaceResourceQuota{
			Limit: &clusterClient.ResourceQuotaLimit{
				RequestsCPU:     defaultZero(nsConfig.ResourceQuota.CPURequests),
				RequestsMemory:  defaultZero(nsConfig.ResourceQuota.MemoryRequests),
				RequestsStorage: defaultZero(nsConfig.ResourceQuota.StorageRequests),
				LimitsCPU:       defaultZero(nsConfig.ResourceQuota.CPULimits),
				LimitsMemory:    defaultZero(nsConfig.ResourceQuota.MemoryLimits),
			},
		},
		ContainerDefaultResourceLimit: &clusterClient.ContainerResourceLimit{
			RequestsCPU:    defaultEmpty(nsConfig.ContainerDefaultResourceLimit.CPURequests),
			RequestsMemory: defaultEmpty(nsConfig.ContainerDefaultResourceLimit.MemoryRequests),
			LimitsCPU:      defaultEmpty(nsConfig.ContainerDefaultResourceLimit.CPULimits),
			LimitsMemory:   defaultEmpty(nsConfig.ContainerDefaultResourceLimit.MemoryLimits),
		},
		Labels: nsConfig.Labels,
	}

	if len(namespaces.Data) == 0 {
		log.Printf("Create namespace %s", ns.Name)
		_, err = cc.Namespace.Create(&ns)
		return err
	}
	log.Printf("Update namespace %s", ns.Name)
	_, err = cc.Namespace.Update(&namespaces.Data[0], &ns)
	return err
}

func defaultEmpty(value string) string {
	if value == "" {
		return ""
	}
	return value
}

func defaultZero(value string) string {
	if value == "" {
		return "0"
	}
	return value
}

// manageRolebindings adds and removes rolebindings from rancher role to ldap group
func manageRolebindings(mc *rancher.MasterClient, clusterName string, clusterID string, project *managementClient.Project, projectConfig *config.Project, groupFormat string) error {

	err := deleteRedundantRoleBindings(mc, clusterName, clusterID, project, projectConfig, groupFormat)
	if err != nil {
		return err
	}
	for _, roleBinding := range projectConfig.Rolebindings {

		for _, group := range roleBinding.Groups {
			roleID, err := mc.GetProjectRoleTemplateID(roleBinding.Name)
			if err != nil {
				return err
			}
			err = manageRolebinding(mc, clusterName, clusterID, project, groupFormat, group, roleBinding.Name, roleID)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func deleteRedundantRoleBindings(mc *rancher.MasterClient, clusterName, clusterID string, project *managementClient.Project, projectConfig *config.Project, groupFormat string) error {
	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"projectId": project.ID,
			"clusterId": clusterID,
		},
	}
	roleBindings, _ := mc.ManagementClient.ProjectRoleTemplateBinding.List(&opts)
	for _, roleBinding := range roleBindings.Data {
		roleBindingRequested, err := roleBindingRequested(mc, roleBinding, projectConfig, groupFormat)
		if err != nil {
			return err
		}
		if roleBinding.RoleTemplateID != "project-owner" && !roleBindingRequested {
			log.Printf("Delete binding for role %q to group %q (project %q cluster %q)", roleBinding.Name, roleBinding.GroupPrincipalID, project.Name, clusterName)
			if err := mc.ManagementClient.ProjectRoleTemplateBinding.Delete(&roleBinding); err != nil {
				return err
			}
		}
	}
	return nil
}

func roleBindingRequested(mc *rancher.MasterClient, existingRoleBinding managementClient.ProjectRoleTemplateBinding, projectConfig *config.Project, groupFormat string) (bool, error) {

	for _, requestedRoleBinding := range projectConfig.Rolebindings {
		requestedRoleTemplateID, err := mc.GetProjectRoleTemplateID(requestedRoleBinding.Name)
		if err != nil {
			return false, err
		}
		if requestedRoleTemplateID == existingRoleBinding.RoleTemplateID {

			for _, group := range requestedRoleBinding.Groups {
				fullGroupName := fmt.Sprintf(groupFormat, group)
				if existingRoleBinding.GroupPrincipalID == fullGroupName {
					return true, nil
				}
			}
		}
	}
	return false, nil
}

func manageRolebinding(mc *rancher.MasterClient, clusterName string, clusterID string, project *managementClient.Project, groupFormat string, groupName string, roleName string, roleID string) error {

	fullGroupName := fmt.Sprintf(groupFormat, groupName)
	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"projectId":        project.ID,
			"clusterId":        clusterID,
			"groupPrincipalId": fullGroupName,
			"roleTemplateId":   roleID,
		},
	}
	roleBindings, _ := mc.ManagementClient.ProjectRoleTemplateBinding.List(&opts)
	if len(roleBindings.Data) > 0 {
		log.Printf("Skip binding for role %q to group %q as it already exists (project %q cluster %q)", roleName, groupName, project.Name, clusterName)
		return nil
	}

	log.Printf("Create binding for role %q to group %q (project %q cluster %q)", roleName, groupName, project.Name, clusterName)
	roleBinding := managementClient.ProjectRoleTemplateBinding{
		ProjectID:        project.ID,
		GroupPrincipalID: fullGroupName,
		RoleTemplateID:   roleID,
	}
	_, err := mc.ManagementClient.ProjectRoleTemplateBinding.Create(&roleBinding)

	return err
}

// manageProject creates or updates a project in the selected cluster
func manageProject(mc *rancher.MasterClient, clusterID string, projectConfig *config.Project, customerName string) (*managementClient.Project, error) {
	filters := map[string]interface{}{
		"name":      projectConfig.Name,
		"clusterId": clusterID,
	}
	projectCollection, err := mc.ManagementClient.Project.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return nil, err
	}
	for _, project := range projectCollection.Data {
		log.Printf("Found %q in cluster %q", project.Name, project.ClusterID)
	}

	project := managementClient.Project{
		Name:      projectConfig.Name,
		ClusterID: clusterID,
		Labels: map[string]string{
			"customer": customerName,
		},
	}

	if !projectConfig.DisableResourceQuota {

		log.Printf("Adding resourcequota to %q in cluster %q", project.Name, project.ClusterID)

		project.ResourceQuota = &managementClient.ProjectResourceQuota{
			Limit: &managementClient.ResourceQuotaLimit{
				RequestsCPU:     defaultZero(projectConfig.ResourceQuota.CPURequests),
				RequestsMemory:  defaultZero(projectConfig.ResourceQuota.MemoryRequests),
				RequestsStorage: defaultZero(projectConfig.ResourceQuota.StorageRequests),
				LimitsCPU:       defaultZero(projectConfig.ResourceQuota.CPULimits),
				LimitsMemory:    defaultZero(projectConfig.ResourceQuota.MemoryLimits),
			},
		}
		project.NamespaceDefaultResourceQuota = &managementClient.NamespaceResourceQuota{
			Limit: initialResourceQuotaLimit(),
		}
	}

	if len(projectCollection.Data) == 0 {
		log.Printf("Create project %q in cluster %q", projectConfig.Name, clusterID)

		newProject, err := mc.ManagementClient.Project.Create(&project)
		if err != nil {
			return nil, err
		}
		return newProject, nil
	}

	log.Printf("Update project %q in cluster %q", projectConfig.Name, clusterID)
	_, err = mc.ManagementClient.Project.Update(&projectCollection.Data[0], &project)
	if err != nil {
		if e, ok := err.(*clientbase.APIError); ok && e.StatusCode == 422 && strings.Contains(e.Body, "MaxLimitExceeded") {
			project.NamespaceDefaultResourceQuota = &managementClient.NamespaceResourceQuota{
				Limit: initialResourceQuotaLimit(),
			}
			_, err = mc.ManagementClient.Project.Update(&projectCollection.Data[0], &project)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}
	return &projectCollection.Data[0], nil
}

func initialResourceQuotaLimit() *managementClient.ResourceQuotaLimit {
	return &managementClient.ResourceQuotaLimit{
		RequestsCPU:     "0",
		RequestsMemory:  "0",
		LimitsCPU:       "0",
		LimitsMemory:    "0",
		RequestsStorage: "0",
	}
}
