package project

import (
	"fmt"
	"log"
	"strings"

	"github.com/rancher/norman/types"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
)

// Creates clusterrolebindings for a customer. The clusterolebindings are rancher objects and labelled for a specific customer.
// The labels are used to cleanup the clusterolebindings once they are not found in the specific customer config.
func manageClusterroleBindings(mc *rancher.MasterClient, customerConfig *config.Customer, allowedClusterRoleCustomers *[]string, groupFormat string) error {

	customerAllowed := false
	for _, allowedClusterRoleCustomer := range *allowedClusterRoleCustomers {
		if allowedClusterRoleCustomer == customerConfig.Name {
			customerAllowed = true
			break
		}
	}
	if !customerAllowed {
		return nil
	}

	rancherClusters, err := mc.ManagementClient.Cluster.ListAll(&types.ListOpts{})
	if err != nil {
		return err
	}
	for _, rancherCluster := range rancherClusters.Data {
		labelFilter := strings.ToLower(fmt.Sprintf("manage-crb-%s", customerConfig.Name))
		existingClusterRoleTemplateBindingArray, err := getCurrentClusterrolebindings(mc, labelFilter, rancherCluster.ID)
		if err != nil {
			return err
		}

		for _, clusterRolebindingsTarget := range customerConfig.Rancher.ClusterRolebindings {
			for _, currentCluster := range clusterRolebindingsTarget.Clusters {
				if currentCluster != rancherCluster.Name {
					continue
				}
				err := createClusterrolebindings(mc, labelFilter, rancherCluster.ID, rancherCluster.Name, clusterRolebindingsTarget, existingClusterRoleTemplateBindingArray, groupFormat)
				if err != nil {
					return err
				}
			}
		}

		err = cleanupClusterrolebindings(mc, rancherCluster.Name, customerConfig, existingClusterRoleTemplateBindingArray, groupFormat)
		if err != nil {
			return err
		}
	}

	return nil
}

func getCurrentClusterrolebindings(mc *rancher.MasterClient, labelFilter string, clusterId string) ([]managementClient.ClusterRoleTemplateBinding, error) {
	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"clusterId": clusterId,
		},
	}
	ClusterRoleTemplateBindingArray := make([]managementClient.ClusterRoleTemplateBinding, 0)
	roletemplateBindingCollection, err := mc.ManagementClient.ClusterRoleTemplateBinding.List(&opts)
	if err != nil {
		return nil, err
	}

	for _, rtb := range roletemplateBindingCollection.Data {
		for k, v := range rtb.Labels {
			if k == "lpc_project_config" && v == labelFilter {
				ClusterRoleTemplateBindingArray = append(ClusterRoleTemplateBindingArray, rtb)
			}
		}
	}
	return ClusterRoleTemplateBindingArray, nil
}

// create new rancher clusterRolebindings for AD group
func createClusterrolebindings(mc *rancher.MasterClient, labelFilter string, clusterId string, clusterName string, clusterRolebindingsTarget config.Rolebinding, existingClusterRoleTemplateBindingArray []managementClient.ClusterRoleTemplateBinding, groupFormat string) error {
	for _, clusterRolebindingGroup := range clusterRolebindingsTarget.Groups {
		groupPrincipalID := fmt.Sprintf(groupFormat, clusterRolebindingGroup)
		exists := false
		for _, existingClusterRoleTemplateBinding := range existingClusterRoleTemplateBindingArray {
			if existingClusterRoleTemplateBinding.RoleTemplateID == clusterRolebindingsTarget.Name && existingClusterRoleTemplateBinding.GroupPrincipalID == groupPrincipalID {
				log.Printf("Role %s exists. Group=%s", clusterRolebindingsTarget.Name, groupPrincipalID)
				exists = true
				break
			}
		}
		if !exists {
			crtb := managementClient.ClusterRoleTemplateBinding{
				ClusterID:        clusterId,
				RoleTemplateID:   clusterRolebindingsTarget.Name,
				GroupPrincipalID: groupPrincipalID,
				Labels: map[string]string{
					"lpc_project_config": labelFilter,
				},
			}
			log.Printf("Created cluster/Role %s/%s. Group=%s", clusterName, clusterRolebindingsTarget.Name, groupPrincipalID)
			_, err := mc.ManagementClient.ClusterRoleTemplateBinding.Create(&crtb)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func cleanupClusterrolebindings(mc *rancher.MasterClient, rancherClusterName string, customerConfig *config.Customer, existingClusterRoleTemplateBindingArray []managementClient.ClusterRoleTemplateBinding, groupFormat string) error {
	for _, existingClusterRoleTemplateBinding := range existingClusterRoleTemplateBindingArray {
		exists := false
		for _, cleanClusterRolebinding := range customerConfig.Rancher.ClusterRolebindings {
			containsCurrentCluster := false
			for _, cleanCluster := range cleanClusterRolebinding.Clusters {
				if cleanCluster == rancherClusterName {
					containsCurrentCluster = true
					break
				}
			}
			if containsCurrentCluster {

				for _, clusterRolebindingGroup := range cleanClusterRolebinding.Groups {
					groupPrincipalID := fmt.Sprintf(groupFormat, clusterRolebindingGroup)
					if existingClusterRoleTemplateBinding.RoleTemplateID == cleanClusterRolebinding.Name && existingClusterRoleTemplateBinding.GroupPrincipalID == groupPrincipalID {
						exists = true
						break
					}
				}
			}
			if exists {
				break
			}
		}

		if !exists {
			log.Printf("Deleting cluster/Role %s/%s. Group=%s", rancherClusterName, existingClusterRoleTemplateBinding.RoleTemplateID, existingClusterRoleTemplateBinding.GroupPrincipalID)
			err := mc.ManagementClient.ClusterRoleTemplateBinding.Delete(&existingClusterRoleTemplateBinding)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
