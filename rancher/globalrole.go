package rancher

// See also https://github.com/rancher/cli/blob/master/cliclient/cliclient.go

import (
	"fmt"

	"github.com/rancher/norman/types"
)

// GetProjectRoleTemplateID gets the ID of a Rancher Project role template
func (mc *MasterClient) GetProjectRoleTemplateID(roleName string) (string, error) {

	opts := types.ListOpts{
		Filters: map[string]interface{}{
			"name":    roleName,
			"context": "project",
		},
	}
	projectRoles, _ := mc.ManagementClient.RoleTemplate.List(&opts)
	if len(projectRoles.Data) > 0 {
		return projectRoles.Data[0].ID, nil
	}

	return "", fmt.Errorf("could not find role %q", roleName)
}
