package rancher

// See also https://github.com/rancher/cli/blob/master/cliclient/cliclient.go

import (
	"strings"

	errorsPkg "github.com/pkg/errors"
	"github.com/rancher/norman/clientbase"
	"github.com/rancher/norman/types"
	clusterClient "github.com/rancher/rancher/pkg/client/generated/cluster/v3"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	projectClient "github.com/rancher/rancher/pkg/client/generated/project/v3"
)

// MasterClient holds the Rancher client for Management client.
type MasterClient struct {
	ManagementClient *managementClient.Client
	TokenKey         string
	URL              string
}

// NewMasterClient returns a new MasterClient with a Management Client
func NewMasterClient(url string, token string) (*MasterClient, error) {
	mc := &MasterClient{
		URL:      url,
		TokenKey: token,
	}

	err := mc.newManagementClient()
	if err != nil {
		return nil, err
	}

	return mc, nil
}

func (mc *MasterClient) newManagementClient() error {
	options := mc.createClientOpts()

	mClient, err := managementClient.NewClient(options)
	if err != nil {
		return err
	}
	mc.ManagementClient = mClient

	return nil
}

// GetClusterID gets the ID of a Cluster
func (mc *MasterClient) GetClusterID(clusterName string) (string, error) {
	filters := map[string]interface{}{
		"name": clusterName,
	}
	clusterCollection, err := mc.ManagementClient.Cluster.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return "", err
	}
	if len(clusterCollection.Data) > 0 {
		return clusterCollection.Data[0].ID, nil
	}
	return "", errorsPkg.Errorf("Cluster %s could not be found", clusterName)
}

// GetClusterName gets the Name of a Cluster
func (mc *MasterClient) GetClusterName(clusterID string) (string, error) {
	filters := map[string]interface{}{
		"id": clusterID,
	}
	clusterCollection, err := mc.ManagementClient.Cluster.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return "", err
	}
	if len(clusterCollection.Data) > 0 {
		return clusterCollection.Data[0].Name, nil
	}
	return "", errorsPkg.Errorf("Cluster %s could not be found", clusterID)
}

// NewClusterClient creates a new ClusterClient for a specific cluster
func (mc *MasterClient) NewClusterClient(clusterName string, clusterID string) (*clusterClient.Client, error) {

	options := mc.createClientOpts()
	options.URL = options.URL + "/clusters/" + clusterID

	cc, err := clusterClient.NewClient(options)
	if err != nil {
		if clientbase.IsNotFound(err) {
			err = errorsPkg.WithMessagef(err, "Cluster %s (%s) is not available", clusterName, clusterID)
		}
		return nil, err
	}
	return cc, nil
}

// NewProjectClient creates a new ProjectClient for a specific project
func (mc *MasterClient) NewProjectClient(projectName string, projectID string) (*projectClient.Client, error) {
	options := mc.createClientOpts()
	options.URL = options.URL + "/projects/" + projectID

	pc, err := projectClient.NewClient(options)
	if err != nil {
		if clientbase.IsNotFound(err) {
			err = errorsPkg.WithMessagef(err, "Project %s (%s) is not available", projectName, projectID)
		}
		return nil, err
	}

	return pc, nil
}

func (mc *MasterClient) createClientOpts() *clientbase.ClientOpts {
	serverURL := mc.URL

	if !strings.HasSuffix(serverURL, "/v3") {
		serverURL = mc.URL + "/v3"
	}

	options := &clientbase.ClientOpts{
		URL:      serverURL,
		TokenKey: mc.TokenKey,
	}
	return options
}
