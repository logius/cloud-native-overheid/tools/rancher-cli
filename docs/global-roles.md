# Project roletemplates

Global > Security > Roles > Projects

| Role name      | Description | Inherit from Role      | Permissions | 
| :---        |    :---- |          :--- | :--- |
| LPC Deployer  | Role for service accounts in pipelines    | project-member | 
| LPC Developer | Developer in DevOps teams | project-member | |
| LPC Namespace Manager | Leader in DevOps teams | - | Resources: namespace Verbs:* APIGroups:* | 
| LPC SecretManager | Secret manager in DevOps teams | secrets-manage |  | 
| LPC Viewer | Readonly account in DevOps teams | read-only |  | 


