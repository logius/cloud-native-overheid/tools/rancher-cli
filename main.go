package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/commands"
)

func main() {
	rootCmd := &cobra.Command{
		SilenceUsage: true,
	}
	commands.AddSubCommands(rootCmd)

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
