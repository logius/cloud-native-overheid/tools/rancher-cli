package group

import (
	"log"
	"os"

	"github.com/rancher/norman/types"
	managementClient "github.com/rancher/rancher/pkg/client/generated/management/v3"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
)

type flags struct {
	rancherURL *string
	token      *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureGlobalRoles(cmd, &flags)
		},
		Use:   "configure-global-roles",
		Short: "Configure Rancher global roles",
		Long:  "This command configures global roles in Rancher.",
	}

	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")

	cmd.MarkFlagRequired("rancher-url")

	return cmd
}

func configureGlobalRoles(cmd *cobra.Command, flags *flags) error {

	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	err = createRoles(mc)
	if err != nil {
		return err
	}

	return nil
}

func createRoles(mc *rancher.MasterClient) error {

	projectRoles := []managementClient.RoleTemplate{
		{
			Name:            "LPC Deployer",
			Context:         "project",
			RoleTemplateIDs: []string{"project-member"},
		},
		{
			Name:            "LPC Developer",
			Context:         "project",
			RoleTemplateIDs: []string{"project-member"},
		},
		{
			Name:    "LPC Namespace Manager",
			Context: "project",
			Rules: []managementClient.PolicyRule{
				{
					Resources: []string{"namespaces"},
					Verbs:     []string{"*"},
					APIGroups: []string{"*"},
				},
			},
		},
		{
			Name:            "LPC SecretManager",
			Context:         "project",
			RoleTemplateIDs: []string{"secrets-manage"},
		},
		{
			Name:            "LPC Viewer",
			Context:         "project",
			RoleTemplateIDs: []string{"read-only"},
		},
	}

	for _, projectRole := range projectRoles {
		err := createRole(mc, &projectRole)
		if err != nil {
			return err
		}
	}
	return nil
}

func createRole(mc *rancher.MasterClient, projectRole *managementClient.RoleTemplate) error {

	filters := map[string]interface{}{
		"builtin": false,
		"context": "project",
		"name":    projectRole.Name,
	}

	rolesCollection, err := mc.ManagementClient.RoleTemplate.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return err
	}
	if len(rolesCollection.Data) > 0 {
		log.Printf("Update globalrole %s", projectRole.Name)
		_, err = mc.ManagementClient.RoleTemplate.Update(&rolesCollection.Data[0], projectRole)
		if err != nil {
			return err
		}
		return nil
	}

	log.Printf("Create globalrole %s", projectRole.Name)
	_, err = mc.ManagementClient.RoleTemplate.Create(projectRole)
	if err != nil {
		return err
	}

	return nil
}
