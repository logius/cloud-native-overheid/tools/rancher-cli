package k8s

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/rancher/norman/types"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// API holds two K8s client interfaces: clientset and dynamic
type API struct {
	Cluster   string
	Clientset *kubernetes.Clientset
	Dynamic   dynamic.Interface
	Config    *rest.Config
	Version   *version.Info
}

func CreateClusterAdminK8sApi(mc *rancher.MasterClient, clusterName string, rancherURL string) (*API, error) {

	parts := strings.Split(clusterName, "-")
	if len(parts) == 6 && strings.EqualFold(parts[5][0:2], "az") /*&& parts[3] == "dev" */ {
		clusterID, _ := mc.GetClusterID(clusterName)
		cluster, err := mc.ManagementClient.Cluster.ByID(clusterID)
		if err != nil {
			return nil, err
		}

		err = deleteOldRancherK8sTokens(mc, clusterID)
		if err != nil {
			return nil, err
		}
		rancherTokenKubeConfig, err := mc.ManagementClient.Cluster.ActionGenerateKubeconfig(cluster)
		if err != nil {
			return nil, err
		}

		time.Sleep(2 * time.Second) //it takes some time for the token to be available in the ace...
		k8sAPI, err := createApiFromConfig(rancherTokenKubeConfig.Config, fmt.Sprintf("https://%s-ace.logius.picardcloud.io", clusterName))
		if err != nil {
			return nil, err
		}
		k8sAPI.Cluster = fmt.Sprintf("%s-%s-%s", parts[3], parts[4], parts[5]) // use short clustername
		version, err := k8sAPI.Clientset.ServerVersion()
		if err != nil {
			return nil, err
		}
		k8sAPI.Version = version
		return k8sAPI, nil
	}
	return nil, nil
}

func deleteOldRancherK8sTokens(mc *rancher.MasterClient, clusterID string) error {
	filters := map[string]interface{}{
		"clusterId":   clusterID,
		"description": "Kubeconfig token",
	}

	tokenColl, err := mc.ManagementClient.Token.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return err
	}
	lastWeek := time.Now().Add(-168 * time.Hour)
	for _, token := range tokenColl.Data {
		if !strings.HasPrefix(token.Description, "Kubeconfig") {
			continue
		}

		tt, err := time.Parse(time.RFC3339, token.Created)
		if err != nil {
			return err
		}
		if tt.Before(lastWeek) {

			err = mc.ManagementClient.Token.Delete(&token)
			if err != nil && !errors.IsNotFound(err) {
				return err
			}
		}
	}
	return nil
}

func createApiFromConfig(stringConfig string, ace string) (*API, error) {

	kubeConfigBytes := []byte(stringConfig)
	config, err := clientcmd.NewClientConfigFromBytes(kubeConfigBytes)

	if err != nil {
		return nil, err
	}
	clientConfig, err := config.ClientConfig()

	if err != nil {
		return nil, err
	}

	raw, _ := config.RawConfig()
	currentContext := raw.CurrentContext
	clientConfig.Host = ace
	log.Printf("K8s currentContext %q, ace=%q", currentContext, ace)

	clientSet := createClientset(clientConfig)
	version, err := clientSet.ServerVersion()
	if err != nil {
		return nil, err
	}

	return &API{
		Cluster:   currentContext,
		Config:    clientConfig,
		Clientset: clientSet,
		Dynamic:   createDynamicClient(clientConfig),
		Version:   version,
	}, nil
}

// CreateAPI initializes a K8s clientset and a dynamic K8s client
func CreateAPI(context string) *API {

	config := getConfig(context)

	clientConfig, err := config.ClientConfig()
	if err != nil {
		log.Fatal(err)
	}

	if context == "" {
		raw, _ := config.RawConfig()
		context = raw.CurrentContext
	}

	log.Printf("K8s context %q", context)
	clientSet := createClientset(clientConfig)
	version, err := clientSet.ServerVersion()
	if err != nil {
		log.Fatal(err)
	}

	return &API{
		Cluster:   context,
		Config:    clientConfig,
		Clientset: clientSet,
		Dynamic:   createDynamicClient(clientConfig),
		Version:   version,
	}
}

// getConfig returns a Kubernetes client config for a given context.
func getConfig(context string) clientcmd.ClientConfig {

	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{}
	if context != "" {
		configOverrides.CurrentContext = context
	}
	return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, configOverrides)
}

// createClientset creates the K8s Client
func createClientset(config *rest.Config) *kubernetes.Clientset {

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	return clientset
}

// CreateDynamicClient creates dynamic K8s Client
func createDynamicClient(config *rest.Config) dynamic.Interface {

	restClient, err := dynamic.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	return restClient
}

func (k8sAPI *API) SecretExists(secretName string, namespace string) (bool, error) {
	_, err := k8sAPI.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// GetSecretData gets the data for a secret
func (k8sAPI *API) GetSecretData(secretName string, namespace string) (map[string][]byte, error) {

	secret, err := k8sAPI.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		fmt.Printf("Secret %s in namespace %s not found\n", secretName, namespace)
	} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
		fmt.Printf("Error getting secret %s in namespace %s: %v\n",
			secretName, namespace, statusError.ErrStatus.Message)
	} else if err != nil {
		return nil, err
	}

	return secret.Data, nil
}
