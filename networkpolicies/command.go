package networkpolicies

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"log"
	"os"
	"strings"

	"github.com/rancher/norman/types"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/rancher"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/yaml"
)

type flags struct {
	config                   *string
	rancherURL               *string
	token                    *string
	proxyCidrs               *[]string
	useRancherK8sApi         *bool
	requiredGlobalPolicyName *string
}

func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureNetworkpolicies(cmd, &flags)
		},
		Use:   "configure-forward-proxy-networkpolicy",
		Short: "Configure forward-proxy network policy ports",
		Long:  "This command configures forward-proxy network policy ports.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.rancherURL = cmd.Flags().String("rancher-url", "", "URL of Rancher")
	flags.token = cmd.Flags().String("rancher-token", os.Getenv("RANCHER_TOKEN"), "Bearer token for Rancher API")
	flags.proxyCidrs = cmd.Flags().StringArray("fw-proxy-cidr", nil, "fw-proxy-cidr")
	flags.useRancherK8sApi = cmd.Flags().Bool("use-rancher-k8sapi", false, "use kubeconfig or rancher k8s api")
	flags.requiredGlobalPolicyName = cmd.Flags().String("required-global-policy-name", "", "If specified, only clusters that contain the policy will be processed")
	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("rancher-url")
	cmd.MarkFlagRequired("fw-proxy-cidr")

	return cmd
}

func configureNetworkpolicies(cmd *cobra.Command, flags *flags) error {

	mc, err := rancher.NewMasterClient(*flags.rancherURL, *flags.token)
	if err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	for _, projectConfig := range customerConfig.Rancher.Projects {
		if len(projectConfig.ForwardProxyPorts) == 0 {
			log.Printf("Skip project %q as there are no forward proxy ports configured", projectConfig.Name)
			continue
		}
		for _, cluster := range projectConfig.Clusters {
			clusterID, err := mc.GetClusterID(cluster)
			if err != nil {
				log.Printf("Skip cluster %q as it could not be found in this Rancher", cluster)
				continue
			}

			err = manageProjectInCluster(flags, mc, cluster, clusterID, &projectConfig, customerConfig.Name, *flags.proxyCidrs)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func manageProjectInCluster(flags *flags, mc *rancher.MasterClient, clusterName string, clusterID string, projectConfig *config.Project, customerName string, fwProxyCidrs []string) error {
	filters := map[string]interface{}{
		"name":      projectConfig.Name,
		"clusterId": clusterID,
	}

	projectCollection, err := mc.ManagementClient.Project.List(&types.ListOpts{Filters: filters})
	if err != nil {
		return err
	}
	for _, project := range projectCollection.Data {
		log.Printf("Found %q in cluster %q", project.Name, project.ClusterID)
		if project.Labels["customer"] != customerName {
			return fmt.Errorf("Customer %s not found in labels from project %s", customerName, project.Name)
		} else {

			var k8sAPI *k8s.API
			if *flags.useRancherK8sApi {
				k8sAPI, err = k8s.CreateClusterAdminK8sApi(mc, clusterName, *flags.rancherURL)
				if err != nil {
					return err
				}

			} else {
				parts := strings.Split(clusterName, "-")
				shortClusterName := strings.Join(parts[3:], "-")
				k8sAPI = k8s.CreateAPI(shortClusterName)
			}

			if *flags.requiredGlobalPolicyName != "" {
				if !lookupGlobalNetworkpolicy(k8sAPI, *flags.requiredGlobalPolicyName) {
					log.Printf("Skip cluster %q because the required global policy is not present on this cluster", clusterName)
					continue
				}
			}

			arr := strings.Split(project.ID, ":")
			projectIdShort := arr[len(arr)-1]

			err = createGlobalNetworkpolicy(k8sAPI, customerName, project.Name, projectIdShort, projectConfig.ForwardProxyPorts, fwProxyCidrs)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func lookupGlobalNetworkpolicy(k8sAPI *k8s.API, policyname string) bool {
	gvr := schema.GroupVersionResource{
		Group:    "crd.projectcalico.org",
		Version:  "v1",
		Resource: "globalnetworkpolicies",
	}
	_, err := k8sAPI.Dynamic.Resource(gvr).Get(context.TODO(), policyname, metav1.GetOptions{})
	return err == nil
}

func createGlobalNetworkpolicy(k8sAPI *k8s.API, customerName string, projectName string, projectIdShort string, forwardProxyPorts []string, fwProxyCidrs []string) error {

	type GlobalNetworkpolicyparams struct {
		Name              string
		ProjectID         string
		ForwardProxyCidrs []string
		ForwardProxyPorts []string
	}

	globalNetworkpolicyparams := GlobalNetworkpolicyparams{
		Name:              strings.ToLower(fmt.Sprintf("lpc-forwardproxy-allow-ports-%s-%s", customerName, projectName)),
		ProjectID:         projectIdShort,
		ForwardProxyCidrs: fwProxyCidrs,
		ForwardProxyPorts: forwardProxyPorts,
	}

	var globalNetworkpolicy unstructured.Unstructured
	fileName := "deployments/networkpolicies/globalnetworkpolicy.yaml"
	if err := parseDeploymentFile(fileName, &globalNetworkpolicyparams, &globalNetworkpolicy); err != nil {
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "crd.projectcalico.org",
		Version:  "v1",
		Resource: "globalnetworkpolicies",
	}

	log.Printf("looking up GlobalNetworkpolicy %q ", globalNetworkpolicy.GetName())
	if existingGlobalNetworkpolicy, err := k8sAPI.Dynamic.Resource(gvr).Get(context.TODO(), globalNetworkpolicy.GetName(), metav1.GetOptions{}); err != nil {
		log.Printf("Create GlobalNetworkpolicy %q", globalNetworkpolicy.GetName())
		_, err = k8sAPI.Dynamic.Resource(gvr).Create(context.TODO(), &globalNetworkpolicy, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		globalNetworkpolicy.SetResourceVersion(existingGlobalNetworkpolicy.GetResourceVersion())
		log.Printf("Update GlobalNetworkpolicy %q", globalNetworkpolicy.GetName())
		_, err = k8sAPI.Dynamic.Resource(gvr).Update(context.TODO(), &globalNetworkpolicy, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}

	return nil
}

func parseDeploymentFile(fileName string, variables interface{}, object *unstructured.Unstructured) error {

	tpl, err := template.ParseFiles(fileName)
	if err != nil {
		tpl, err = template.ParseFiles("/" + fileName)
		if err != nil {
			return err
		}
	}

	var buf bytes.Buffer
	if err := tpl.Execute(&buf, variables); err != nil {
		return err
	}

	if err := yaml.Unmarshal(buf.Bytes(), object); err != nil {
		return err
	}

	return nil
}
