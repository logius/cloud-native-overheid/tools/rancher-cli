package gitlabclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/xanzy/go-gitlab"
)

func variableExists(gitlabClient *gitlab.Client, group *gitlab.Group, key string, scope string) (bool, error) {
	opt := &gitlab.ListGroupVariablesOptions{Page: 1}

	for opt.Page > 0 {
		variables, r, err := gitlabClient.GroupVariables.ListVariables(group.ID, opt)
		if err != nil {
			return false, err
		}

		for _, variable := range variables {
			if variable.Key == key && variable.EnvironmentScope == scope {
				return true, err
			}
		}

		opt.Page = r.NextPage
	}
	return false, nil
}

// UpdateGroupVariable creates or updates an environment variable in a GitLab Group
func UpdateGroupVariable(gitlabClient *gitlab.Client, group *gitlab.Group, key string, value string, scope string, variableType gitlab.VariableTypeValue) error {

	groupVarExists, err := variableExists(gitlabClient, group, key, scope)
	if err != nil {
		return err
	}

	if groupVarExists {
		// Gitlab API is unable to delete or update variables with the same keyname and different scopes, see  https://gitlab.com/gitlab-org/gitlab/-/issues/340185.
		// Instead we delete the variable with a custom api server.
		// Updating the value with the custom api server has been considered, but the value has to be encrypted, and that would be quite a difficult task.
		baseUrl := gitlabClient.BaseURL()
		apiUrl := fmt.Sprintf("%s://%s/api/extra", baseUrl.Scheme, baseUrl.Host)
		if err := deleteVariable(apiUrl, group, key, scope); err != nil {
			return err
		}
	}

	log.Printf("Create variable %q for scope %q in group %s", key, scope, group.FullPath)
	masked := false
	opt := &gitlab.CreateGroupVariableOptions{
		EnvironmentScope: &scope,
		Key:              &key,
		Masked:           &masked,
		Value:            &value,
		VariableType:     &variableType,
	}
	_, _, err = gitlabClient.GroupVariables.CreateVariable(group.ID, opt)
	if err != nil {
		return fmt.Errorf("failed to CreateVariable: %v", err)
	}

	return nil
}

const jsonContentType = "application/json"

func deleteVariable(apiUrl string, group *gitlab.Group, key string, scope string) error {

	type GroupVar struct {
		GroupID          int    `json:"group_id"`
		Key              string `json:"key"`
		EnvironmentScope string `json:"environment_scope"`
	}

	groupVar := GroupVar{
		GroupID:          group.ID,
		Key:              key,
		EnvironmentScope: scope,
	}
	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(groupVar)

	url := fmt.Sprintf("%s/groups/variables", apiUrl)

	log.Printf("Custom API Server %v", url)
	log.Printf("Delete variable %q for scope %q in group %s", key, scope, group.FullPath)
	req, err := http.NewRequest(http.MethodDelete, url, buf)
	if err != nil {
		return fmt.Errorf("error while creating a new http request for \"%s\": %v", url, err)
	}
	req.Header.Set("gitlab-access-token", os.Getenv("GITLAB_ACCESS_TOKEN"))
	req.Header.Set("Content-Type", jsonContentType)

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error while performing http request: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		responseData, _ := ioutil.ReadAll(res.Body)
		return fmt.Errorf("could not delete group variable, status code: %d responseData: %v", res.StatusCode, string(responseData))
	}

	return nil
}
