package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// GitLabGroupConfig spec
type GitLabGroupConfig struct {
	Name string `validate:"required"`
}

// GitLab configuration
type GitLab struct {
	// KubeConfigVariables = true: KUBECONFIG variables are created and k8s clusters are removed
	// KubeConfigVariables = false: k8s clusters are created
	//KubeConfigVariables bool                `yaml:"kubeConfigVariables"`
	Groups []GitLabGroupConfig `validate:"required,dive"`
}

// Rolebinding binds a K8s role to an IAM group.
type Rolebinding struct {
	Name     string   `validate:"required"`
	Clusters []string `yaml:"clusters,omitempty"`
	Groups   []string `validate:"required,dive" yaml:"groups,omitempty"`
}

// ResourceQuota defines the requests and limits on resources
type ResourceQuota struct {
	CPULimits       string `validate:"required" yaml:"limits.cpu,omitempty"`
	MemoryLimits    string `validate:"required" yaml:"limits.memory,omitempty"`
	CPURequests     string `validate:"required" yaml:"requests.cpu,omitempty"`
	MemoryRequests  string `validate:"required" yaml:"requests.memory,omitempty"`
	StorageRequests string `validate:"required" yaml:"requests.storage,omitempty"`
}

// Group configuration
type Group struct {
	Name string `validate:"required"`
}

// Namespace defines the K8s namespace for a customer.
type Namespace struct {
	Name                          string
	ResourceQuota                 ResourceQuota `validate:"required" yaml:"resourcequota,omitempty"`
	ContainerDefaultResourceLimit ResourceQuota `yaml:"containerDefaultResourceLimit,omitempty"`
	Labels                        map[string]string
}

// Project configuration
type Project struct {
	Name                     string        `validate:"required"`
	Clusters                 []string      `validate:"required"`
	ForwardProxyPorts        []string      `validate:"required" yaml:"forwardProxyPorts"`
	Namespaces               []Namespace   `yaml:"namespaces,omitempty"`
	Rolebindings             []Rolebinding `validate:"required,dive" yaml:"roles"`
	ResourceQuota            ResourceQuota `validate:"required" yaml:"resourcequota"`
	DisableResourceQuota     bool          `yaml:"disableResourceQuota"`     // if resource quota should be set
	DisableServicesNamespace bool          `yaml:"disableServicesNamespace"` // if a services namespace should be created
}

type Nginx struct {
	RancherProject string   `validate:"required" yaml:"rancherproject"`
	Clusters       []string `validate:"required"`
}

type DataStream struct {
	Name   string `validate:"required"`
	Policy string // default short
	Nginx  Nginx
}

type Elastic struct {
	CrossClusterReplication bool `yaml:"crossClusterReplication"`
	DataStreams             []DataStream
}

type SocLog struct {
	Topic           string   `validate:"required"`
	RancherProjects []string `validate:"required,dive"`
}

// Rancher configuration
type Rancher struct {
	ClusterRolebindings []Rolebinding `yaml:"clusterroles,omitempty"`
	Projects            []Project     `validate:"required,dive"`
}

// Customer configuration
type Customer struct {
	Name        string   `validate:"required"`
	Rancher     Rancher  `validate:"required"`
	Groups      []Group  `validate:"required,dive"`
	GitLab      GitLab   `validate:"required"`
	ElasticTest *Elastic `yaml:"elastic-test"`
	ElasticProd *Elastic `yaml:"elastic-prod"`
	SocLogging  []SocLog `yaml:"soc-logging" validate:"required"`
}

// Config contains customer configuration
type Config struct {
	Customer Customer `validate:"required"`
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}
